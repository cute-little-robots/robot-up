import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import ConnectionModel from './connection';
import { getLogger } from '../utils/logger';
import SocketManager from '../data-access/socket-manager';

const _logger = getLogger('TerminalModel');
const MAX_LINES = 500;

/**
 * Models an interactive terminal that allows the exchange of text based
 * messages with the remote WebREPL host.
 */
class TerminalModel {
    _connection = undefined;
    _lines = undefined;
    _maxLines = 20;

    /**
     * @param {Object} socketManager An abstract class that allows the
     *        creation/management of a socket connection.
     * @param {Object} connection A reference to the connection model that can
     *        be used to check for connection status.
     */
    constructor(socketManager, connection) {
        _logger.silly('ctor()', socketManager, connection);

        _argValidator.checkInstance(
            socketManager,
            SocketManager,
            'Invalid socket manager (arg #1)'
        );
        _argValidator.checkInstance(
            connection,
            ConnectionModel,
            'Invalid connection model (arg #2)'
        );

        this._socketManager = socketManager;
        this._connection = connection;
        this._lines = [];

        this._socketManager.on(
            'stringData',
            this._processTerminalData.bind(this)
        );

        this._socketManager.on(
            'close',
            action('onSocketClose', () => {
                _logger.silly('onSocketClose()');
                this.clear();
            })
        );
    }

    /**
     * Gets a list of response lines from the remote repl host. All text based
     * data returned by the host is broken up by the newline character into
     * lines.
     *
     * @type {Array}
     */
    get lines() {
        return this._lines;
    }

    /**
     * Determines if the terminal is ready for use.
     *
     * @type {Boolean}
     */
    get isReady() {
        return this._connection.isReady;
    }

    /**
     * Processes string data received from the repl host. This typically
     * consists of responses to REPL commands issued by the client.
     *
     * @private
     * @param {String} data String data returned by the REPL client.
     */
    _processTerminalData(data) {
        _logger.silly('_processTerminalData()', data && data.length);

        const currentLine = this._lines.pop() || '';
        const lines = data.split('\n');
        const firstLine = lines.shift();

        this._lines.push(currentLine + firstLine);
        lines.forEach((line) => {
            this._lines.push(line);
        });

        const extra = this._lines.length - MAX_LINES;
        for (let index = 0; index < extra; index++) {
            this._lines.shift();
        }
    }

    /**
     * Sends data to the REPL host.
     *
     * @param {String} data The data to send to the REPL host.
     */
    sendData(data) {
        _logger.silly('sendData()', data && data.length);

        _argValidator.checkString(data, 0, 'Invalid data (arg #1)');
        if (!this.isReady) {
            _logger.warn('Cannot send data - connection is not ready');
            return;
        }

        _logger.trace('Sending data to remote host');
        this._socketManager.sendData(data);
    }

    /**
     * Sends a run command to the REPL host.
     */
    sendRunCommand() {
        _logger.silly('sendRunCommand()');

        if (!this.isReady) {
            _logger.warn('Cannot send data - connection is not ready');
            return;
        }

        _logger.trace('Sending run command to remote host');
        this._socketManager.sendData('system.run()\r');
    }

    /**
     * Sends a stop command to the REPL host.
     */
    sendStopCommand() {
        _logger.silly('sendStopCommand()');

        if (!this.isReady) {
            _logger.warn('Cannot send data - connection is not ready');
            return;
        }

        _logger.trace('Sending stop command to remote host');
        this._socketManager.sendData('\x03');
    }

    /**
     * Clears the terminal buffer.
     */
    clear() {
        _logger.silly('clear()');

        this._lines = [];
    }
}

export default decorate(TerminalModel, {
    _connection: observable,
    _lines: observable,

    lines: computed,

    _processTerminalData: action,
    sendData: action,
    clear: action
});

import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import ReplSystem from '../data-access/repl-system';

import { getLogger } from '../utils/logger';

const _logger = getLogger('FileModel');

/**
 * Represents the contents of a file on the remote repl host. This file can be
 * read from and written to. Different source/destination file names are
 * supported.
 */
class FileModel {
    _contents = '';
    _sourceFile = '';
    _destinationFile = '';
    _errorMessage = '';
    _isBusy = false;
    _isReady = false;

    /**
     * @param {Object} replSystem Reference to the repl system component that
     *        allows interaction with the remote host over the WebREPL protocol.
     */
    constructor(sourceFile, destinationFile, replSystem) {
        _logger.silly('ctor()', sourceFile, destinationFile, replSystem);

        _argValidator.checkString(sourceFile, 1, 'Invalid sourceFile (arg #1)');
        _argValidator.checkString(
            destinationFile,
            1,
            'Invalid destinationFile (arg #2)'
        );
        _argValidator.checkInstance(
            replSystem,
            ReplSystem,
            'Invalid replSystem (arg #3)'
        );

        this._sourceFile = sourceFile;
        this._destinationFile = destinationFile;
        this._isBusy = false;
        this._isReady = false;
        this._errorMessage = '';
        this._contents = '';
        this._replSystem = replSystem;
    }

    /**
     * Returns the contents of the file.
     *
     * @type {String}
     */
    get contents() {
        return this._contents;
    }

    /**
     * Returns the error message associated any operations performed by the
     * model.
     *
     * @type {String}
     */
    get errorMessage() {
        return this._errorMessage;
    }

    /**
     * Returns a flag that indicates that the model is busy with an i/o
     * operation.
     *
     * @type {Boolean}
     */
    get isBusy() {
        return this._isBusy;
    }

    /**
     * Returns a flag that indicates that the model is ready, with all data
     * loaded from the remote repl host.
     *
     * @type {Boolean}
     */
    get isReady() {
        return this._isReady;
    }

    /**
     * Returns a boolean flag that indicates whether or not the model reported
     * any errors.
     *
     * @type {Boolean}
     */
    get hasErrors() {
        return this._errorMessage.length > 0;
    }

    /**
     * Loads data from the source file. This method will take no action if data
     * has already been loaded, unless the force flag is set.
     *
     * @param {Boolean} force A flag that forces a load of data even if data
     *        was previously read, and exists in cache.
     * @returns {Object} A promise that is rejected/resolved based on the
     *          outcome of the operation.
     */
    load(force) {
        _logger.silly('load()', force);

        if (this._isReady && !force) {
            _logger.debug('Contents exist in cache. Skipping fetch');
            return;
        }

        if (this._isBusy) {
            _logger.warn('Load already in progress. Skipping');
            return;
        }

        _logger.trace('Fetching data from remote host', this._sourceFile);

        this._isBusy = true;
        this._errorMessage = '';

        return this._replSystem.readFile(this._sourceFile).then(
            action('readFileSuccess', (data) => {
                _logger.silly('readFileSuccess()');

                _logger.trace('File data loaded successfully');

                this._contents = data;
                this._isBusy = false;
                this._isReady = true;
                this._errorMessage = '';
            }),
            action('readFileFailure', (err) => {
                _logger.silly('readFileFailure()');

                _logger.error('Error loading file data', err.toString());

                this._isBusy = false;
                this._errorMessage = err.toString();
            })
        );
    }

    /**
     * Saves data to the destination file.
     *
     * @returns {Object} A promise that is rejected/resolved based on the
     *          outcome of the operation.
     */
    save() {
        _logger.silly('save()');

        if (this._isBusy) {
            _logger.warn('Save already in progress. Skipping');
            return;
        }

        _logger.trace('Saving data to remote host', this._destinationFile);

        this._isBusy = true;
        this._errorMessage = '';

        return this._replSystem
            .writeFile(this._destinationFile, this._contents)
            .then(
                action('writeFileSuccess', () => {
                    _logger.silly('writeFileSuccess()');

                    _logger.trace('File data saved successfully');

                    this._isBusy = false;
                    this._errorMessage = '';
                }),
                action('writeFileFailure', (err) => {
                    _logger.silly('writeFileFailure()', err);

                    _logger.error('Error writing file data', err.toString());

                    this._isBusy = false;
                    this._errorMessage = err.toString();
                })
            );
    }

    /**
     * Sets the file contents.
     *
     * @param {String} contents The contents to set for the file.
     */
    setContents(contents) {
        _logger.silly('setContents()', contents && contents.length);

        _argValidator.checkString(contents, 0, 'Invalid contents (arg #1)');
        this._contents = contents;
    }
}

export default decorate(FileModel, {
    _contents: observable,
    _errorMessage: observable,
    _isBusy: observable,
    _isReady: observable,

    contents: computed,
    errorMessage: computed,
    isBusy: computed,
    isReady: computed,

    hasErrors: computed,

    setContents: action,
    load: action,
    save: action
});

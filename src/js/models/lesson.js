import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import SectionModel from './section';
import FileModel from './file';
import ReplSystem from '../data-access/repl-system';

import { getLogger } from '../utils/logger';

const _logger = getLogger('LessonModel');

const STATE_NOT_STARTED = 0;
const STATE_IN_PROGRESS = 1;
const STATE_COMPLETED = 2;
const DEFAULT_DEST_FILE = 'program.py';

/**
 * Represents a single lesson that a student can complete. Each lesson includes
 * multiple sections, and is only complete when the sections have all been
 * completed.
 */
class LessonModel {
    _id = -1;
    _sourceFile = '';
    _code = undefined;
    _title = '';
    _sections = [];
    _currentSectionId = -1;
    _lessonState = STATE_NOT_STARTED;
    _replSystem = undefined;
    _errorMessage = '';
    _isBusy = false;
    _isReady = false;
    _lessonDetailsFile = '';

    /**
     * @param {String} id The id of the lesson.
     * @param {Object} replSystem Reference to the repl system component that
     *        allows interaction with the remote host over the WebREPL protocol.
     */
    constructor(id, replSystem) {
        _logger.silly('ctor()', id, replSystem);

        _argValidator.checkNumber(id, -1, 'Invalid id (arg #1)');
        _argValidator.checkInstance(
            replSystem,
            ReplSystem,
            'Invalid replSystem (arg #2)'
        );

        this._id = id;
        this._title = '';
        this._sections = [];
        this._code = undefined;
        this._currentSectionId = -1;
        this._lessonState = STATE_NOT_STARTED;
        this._replSystem = replSystem;
        this._errorMessage = '';
        this._isBusy = false;
        this._isReady = false;
        this._lessonDetailsFile = '';
    }

    /**
     * The id of the lesson.
     *
     * @type {String}
     */
    get id() {
        return this._id;
    }

    /**
     * The title of the lesson.
     *
     * @type {String}
     */
    get title() {
        return this._title;
    }

    /**
     * The name of the source file associated with the lesson.
     *
     * @type {String}
     */
    get sourceFile() {
        return this._sourceFile;
    }

    /**
     * Returns a model that allows the management of source code associated with
     * the lesson.
     *
     * @type {Object}
     */
    get code() {
        return this._code;
    }

    /**
     * Returns an array of sections that make up the lesson.
     *
     * @type {Array}
     */
    get sections() {
        return this._sections;
    }

    /**
     * Reference to the currently active section. If no active section exists,
     * this value will be undefined.
     *
     * @type {Object}
     */
    get currentSection() {
        return this._sections.length > this._currentSectionId
            ? this._sections[this._currentSectionId]
            : undefined;
    }

    /**
     * The id of the currently active section.
     *
     * @type {Number}
     */
    get currentSectionId() {
        return this._currentSectionId;
    }

    /**
     * Determines if the lesson has not yet started.
     *
     * @type {Boolean}
     */
    get notStarted() {
        return this._lessonState === STATE_NOT_STARTED;
    }

    /**
     * Determines if the lesson has been completed.
     *
     * @type {Boolean}
     */
    get isComplete() {
        return this._lessonState === STATE_COMPLETED;
    }

    /**
     * Determines if the lesson has a next section available based on the
     * current section for the lesson.
     *
     * @return {Boolean} true if a next section is available, false otherwise.
     */
    get hasNextSection() {
        return (
            this._sections.length > 0 &&
            this.currentSectionId < this._sections.length - 1
        );
    }

    /**
     * Determines if the lesson has a previous section available based on the
     * current section for the lesson.
     *
     * @return {Boolean} true if a previous section is available, false otherwise.
     */
    get hasPreviousSection() {
        return this._sections.length > 0 && this.currentSectionId > 0;
    }

    /**
     * The name of the file that contains lesson details.
     *
     * @type {String}
     */
    get lessonDetailsFile() {
        return this._lessonDetailsFile;
    }

    /**
     * Returns the error message associated any operations performed by the
     * model.
     *
     * @type {String}
     */
    get errorMessage() {
        return this._errorMessage;
    }

    /**
     * Returns a flag that indicates that the model is busy with an i/o
     * operation.
     *
     * @type {Boolean}
     */
    get isBusy() {
        return this._isBusy;
    }

    /**
     * Returns a boolean flag that indicates whether or not the model reported
     * any errors.
     *
     * @type {Boolean}
     */
    get hasErrors() {
        return this._errorMessage.length > 0;
    }

    /**
     * Sets the properties of the lesson using the specified JSON object. This
     * is intended for internal consumption within the models, and should not be
     * invoked directly by the UI.
     *
     * @package (internal consumption only)
     * @param {Object} props An object that defines the properties of the lesson.
     *
     * @returns {Object} A reference to the current object.
     */
    _deserialize(props) {
        _logger.silly('_deserialize()', props);

        _argValidator.checkObject(props, 'Invalid props (arg #1)');
        _argValidator.checkString(
            props.title,
            1,
            'Invalid title (props.title)'
        );
        _argValidator.checkString(
            props.sourceFile,
            1,
            'Invalid title (props.sourceFile)'
        );
        _argValidator.checkString(
            props.lessonDetailsFile,
            'Invalid lessonDetailsFile (props.lessonDetailsFile)'
        );
        _argValidator.checkNumber(
            props.lessonState,
            0,
            'Invalid lessonState (props.lessonState)'
        );
        _argValidator.checkNumber(
            props.currentSectionId,
            -1,
            'Invalid currentSectionId (props.currentSectionId)'
        );

        this._title = props.title;
        this._sourceFile = props.sourceFile;
        this._lessonState = props.lessonState;
        this._lessonDetailsFile = props.lessonDetailsFile;

        this._code = new FileModel(
            this._sourceFile,
            DEFAULT_DEST_FILE,
            this._replSystem
        );

        this._currentSectionId = props.currentSectionId;

        this._tmp__sections = [];
        // this._sections = props.sections.map((sectionData) =>
        //     new SectionModel(sectionData.id)._deserialize(sectionData)
        // );

        return this;
    }

    /**
     * Gets the properties of the lesson in JSON format. This is intended for
     * internal consumption within the models, and should not be invoked
     * directly by the UI.
     *
     * @package (internal consumption only)
     * @returns {Object} object that defines the properties of the lesson.
     */
    _serialize() {
        _logger.silly('_serialize()');

        return {
            id: this._id,
            title: this._title,
            sourceFile: this._sourceFile,
            lessonState: this._lessonState,
            sections: this._sections.map((section) => section._serialize()),
            currentSectionId: this._currentSectionId
        };
    }

    /**
     * Starts the current lesson. This is intended for internal consumption
     * within the models, and should not be invoked directly by the UI.
     *
     * @package (internal consumption only)
     */
    _start() {
        _logger.silly('_start()');

        if (this._lessonState !== STATE_NOT_STARTED) {
            throw new Error('Lesson cannot be started from current state');
        }

        _logger.trace('Starting lesson', this.id);
        this._lessonState = STATE_IN_PROGRESS;
    }

    /**
     * Loads lesson details (section information) from the remote repl host.
     *
     * @param {Boolean} force A flag that forces a load of data even if data
     *        was previously read, and exists in cache.
     * @returns {Object} A promise that is rejected/resolved based on the
     *          outcome of the operation.
     */
    loadDetails(force) {
        _logger.silly('load()', force);

        if (this._isReady && !force) {
            _logger.debug('Lesson details exist in cache. Skipping fetch');
            return;
        }

        if (this._isBusy) {
            _logger.warn('Load already in progress. Skipping');
            return;
        }

        _logger.trace('Fetching lesson details from remote host');

        this._isBusy = true;
        this._errorMessage = '';

        return this._replSystem.readFile(this._lessonDetailsFile).then(
            action('lessonDetailsLoadSuccess', (data) => {
                _logger.silly('lessonDetailsLoadSuccess()', data);

                this._sections = this._tmp__sections.map((sectionData) =>
                    new SectionModel(sectionData.id)._deserialize(sectionData)
                );
                try {
                    const sections = JSON.parse(data).sections;
                    this._sections = sections.map((sectionData) =>
                        new SectionModel(sectionData.id)._deserialize(
                            sectionData
                        )
                    );
                    this._errorMessage = '';
                    this._isReady = true;
                } catch (ex) {
                    _logger.error(
                        'Error parsing lesson details data',
                        ex.toString()
                    );
                    this._errorMessage = ex.toString();
                }
                this._isBusy = false;
            }),
            action('lessonDetailsLoadFailure', (err) => {
                _logger.silly('lessonDetailsLoadFailure()', err);
                _logger.error('Error writing file data', err.toString());

                this._isBusy = false;
                this._errorMessage = err.toString();
            })
        );
    }

    /**
     * Changes the section selection to the next section. This method will
     * have no effect if the currently selected section is the last section.
     */
    selectNextSection() {
        _logger.silly('selectNextSection()');

        if (this._sections.length <= 0) {
            _logger.warn('There are no sections to navigate');
            return;
        }

        if (this._currentSectionId < this._sections.length - 1) {
            this._currentSectionId++;
            _logger.debug('Section selected', this._currentSectionId);

            if (this._currentSectionId === this._sections.length - 1) {
                this._lessonState = STATE_COMPLETED;
                _logger.debug('Lesson completed', this._id);
            }
        } else {
            _logger.warn('Cannot go beyond last section');
        }
    }

    /**
     * Changes the section selection to the previous section. This method will
     * have no effect if the currently selected section is the first section.
     */
    selectPreviousSection() {
        _logger.silly('selectPreviousSection()');

        if (this._sections.length <= 0) {
            _logger.warn('There are no sections to navigate');
            return;
        }

        if (this._currentSectionId > 0) {
            this._currentSectionId--;
            _logger.debug('Section selected', this._currentSectionId);
        } else {
            _logger.warn('Cannot go prior to first section');
        }
    }
}

export default decorate(LessonModel, {
    _errorMessage: observable,
    _isBusy: observable,
    _id: observable,
    _title: observable,
    _sourceFile: observable,
    _code: observable,
    _lessonState: observable,
    _sections: observable,
    _currentSectionId: observable,
    _lessonDetailsFile: observable,

    errorMessage: computed,
    isBusy: computed,
    id: computed,
    title: computed,
    sourceFile: computed,
    code: computed,
    sections: computed,
    currentSection: computed,
    currentSectionId: computed,
    notStarted: computed,
    isComplete: computed,
    hasNextSection: computed,
    hasPreviousSection: computed,
    lessonDetailsFile: computed,

    hasErrors: computed,

    _deserialize: action,
    _start: action,
    load: action,
    selectNextSection: action,
    selectPreviousSection: action
});

import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import { getLogger } from '../utils/logger';
import SocketManager from '../data-access/socket-manager';

const _logger = getLogger('ConnectionModel');

/**
 * Models a connection to a remote WebREPL host.
 */
class ConnectionModel {
    _errorMessage = '';
    _isConnecting = false;
    _isConnected = false;

    /**
     * @param {Object} socketManager An abstract class that allows the
     *        creation/management of a socket connection.
     */
    constructor(socketManager) {
        _logger.silly('ctor()', socketManager);

        _argValidator.checkInstance(
            socketManager,
            SocketManager,
            'Invalid socket manager (arg #1)'
        );

        this._socketManager = socketManager;
        this._isConnected = false;
        this._isConnecting = false;
        this._errorMessage = '';

        _logger.trace('Setting up event handlers');

        this._socketManager.on(
            'open',
            action('onSocketOpen', () => {
                _logger.silly('onSocketOpen()');
                this._isConnected = true;
                this._isConnecting = false;
            })
        );

        this._socketManager.on(
            'error',
            action('onSocketError', (err) => {
                _logger.silly('onSocketError()', err);

                if (this._isConnecting) {
                    this._errorMessage =
                        'Error opening connection to remote host';
                } else {
                    _logger.trace(
                        'Ignoring error on already established connection'
                    );
                }
            })
        );

        this._socketManager.on(
            'close',
            action('onSocketClose', () => {
                _logger.silly('onSocketClose()');
                this._isConnected = false;
                this._isConnecting = false;
            })
        );
    }

    /**
     * Gets the error message associated with any connection errors.
     *
     * @type {String}
     */
    get errorMessage() {
        return this._errorMessage;
    }

    /**
     * Determines if connection to the remote host is ready for use.
     *
     * @type {Boolean}
     */
    get isReady() {
        return this._isConnected;
    }

    /**
     * Determines if a connection to the WebRepl host is in progress.
     *
     * @type {Boolean}
     */
    get isConnecting() {
        return this._isConnecting;
    }

    /**
     * Initializes a connection to the WebRepl host.
     *
     * @param {String} url The url of the remote host to connect to.
     * @param {String} password The password to send to the remote host after
     *        connection.
     */
    open(url, password) {
        _logger.silly('open()', url, password);

        if (!_argValidator.checkString(url, 1)) {
            _logger.warn('Cannot open connection with empty url string');
            return;
        }

        if (!_argValidator.checkString(password, 1)) {
            _logger.warn('Cannot open connection with empty password');
            return;
        }

        if (this._isConnected) {
            _logger.warn('Socket has already been initialized');
            return;
        }

        _logger.trace('Opening connection');
        this._errorMessage = '';
        this._isConnecting = true;
        this._socketManager.connect(url, password);
    }

    /**
     * Closes the connection to the WebRepl host.
     */
    close() {
        _logger.silly('close()');

        if (!this._isConnected) {
            _logger.warn('Cannot close connection - socket is not ready');
            return;
        }

        _logger.trace('Closing connection');
        this._socketManager.close();
    }
}

export default decorate(ConnectionModel, {
    _errorMessage: observable,
    _isConnected: observable,
    _isConnecting: observable,

    errorMessage: computed,
    isReady: computed,
    isConnecting: computed,

    open: action,
    close: action
});

import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import LessonModel from './lesson';
import ReplSystem from '../data-access/repl-system';

import { getLogger } from '../utils/logger';

const _logger = getLogger('LessonListModel');

const LESSONS_FILE_NAME = 'lessons.json';
const PROGRESS_FILE_NAME = 'progress.json';

/**
 * Represents a list of lessons that are available to the student.
 */
class LessonListModel {
    _errorMessage = '';
    _isBusy = false;
    _isReady = false;

    _lessons = [];
    _currentLessonId = -1;
    _replSystem = undefined;

    /**
     * @param {Object} replSystem Reference to the repl system component that
     *        allows interaction with the remote host over the WebREPL protocol.
     */
    constructor(replSystem) {
        _logger.silly('ctor()', replSystem);

        _argValidator.checkInstance(
            replSystem,
            ReplSystem,
            'Invalid replSystem (arg #1)'
        );

        this._isBusy = false;
        this._errorMessage = '';
        this._lessons = [];
        this._currentLessonId = -1;
        this._replSystem = replSystem;
        this._isReady = false;
    }

    /**
     * Returns the error message associated any operations performed by the
     * model.
     *
     * @type {String}
     */
    get errorMessage() {
        return this._errorMessage;
    }

    /**
     * Returns a flag that indicates that the model is busy with an i/o
     * operation.
     *
     * @type {Boolean}
     */
    get isBusy() {
        return this._isBusy;
    }

    /**
     * Returns a flag that indicates that the model is ready, with all data
     * loaded from the remote repl host.
     *
     * @type {Boolean}
     */
    get isReady() {
        return this._isReady;
    }

    /**
     * Returns a boolean flag that indicates whether or not the model reported
     * any errors.
     *
     * @type {Boolean}
     */
    get hasErrors() {
        return this._errorMessage.length > 0;
    }

    /**
     * The list of lessons.
     *
     * @type {Array}
     */
    get lessons() {
        return this._lessons;
    }

    /**
     * The id of the currently active lesson.
     *
     * @type {Number}
     */
    get currentLessonId() {
        return this._currentLessonId;
    }

    /**
     * Reference to the currently active lesson. If no active section exists,
     * this value will be undefined.
     *
     * @type {Object}
     */
    get currentLesson() {
        return this._lessons[this._currentLessonId];
    }

    /**
     * Determines if the lesson has a next lesson available based on the
     * current lesson for the lesson.
     *
     * @return {Boolean} true if a next lesson is available, false otherwise.
     */
    get hasNextLesson() {
        return (
            this._lessons.length > 0 &&
            this.currentLessonId < this._lessons.length - 1
        );
    }

    /**
     * Determines if the lesson has a previous lesson available based on the
     * current lesson for the lesson.
     *
     * @return {Boolean} true if a previous lesson is available, false otherwise.
     */
    get hasPreviousLesson() {
        return this._lessons.length > 0 && this.currentLessonId > 0;
    }

    /**
     * Sets the properties of the lesson list using the specified JSON object.
     * This is intended for internal consumption within the models, and should
     * not be invoked directly by the UI.
     *
     * @package (internal consumption only)
     * @param {Object} props An object that defines the properties of the
     *        lesson list.
     *
     * @returns {Object} A reference to the current object.
     */
    _deserialize(props) {
        _logger.silly('_deserialize()', props);

        _argValidator.checkObject(props, 'Invalid props (arg #1)');
        _argValidator.checkObject(
            props.lessonData,
            'Invalid lessonData (props.lessonData)'
        );
        _argValidator.checkObject(
            props.progressData,
            'Invalid progressData (props.progressData)'
        );
        _argValidator.checkNumber(
            props.progressData.currentLessonId,
            -1,
            'Invalid currentLessonId (props.currentLessonId)'
        );

        const { lessonData, progressData } = props;

        // Combine data and props so that child objects can be created.
        const combinedData = lessonData.lessons.map((lesson) => {
            const lessonProgress = progressData.lessons[lesson.id];
            const { lessonState, currentSectionId } = lessonProgress;

            return Object.assign({}, lesson, {
                lessonState,
                currentSectionId
            });
        });

        // Initialize child (lesson and section) objects.
        this._lessons = combinedData.map((lesson) =>
            new LessonModel(lesson.id, this._replSystem)._deserialize(lesson)
        );

        // Set properties required by current object
        this._currentLessonId = progressData.currentLessonId;

        return this;
    }

    /**
     * Loads a lesson list from the remote repl host.
     *
     * @param {Boolean} force A flag that forces a load of data even if data
     *        was previously read, and exists in cache.
     * @returns {Object} A promise that is rejected/resolved based on the
     *          outcome of the operation.
     */
    load(force) {
        _logger.silly('load()', force);

        if (this._isReady > 0 && !force) {
            _logger.debug('Lessons exist in cache. Skipping fetch');
            return;
        }

        if (this._isBusy) {
            _logger.warn('Load already in progress. Skipping');
            return;
        }

        _logger.trace('Fetching lessons from remote host');

        this._isBusy = true;
        this._errorMessage = '';

        let lessonData = {};

        return this._replSystem
            .readFile(LESSONS_FILE_NAME)
            .then((data) => {
                _logger.silly('lessonLoadSuccess()', data);

                lessonData = data;

                _logger.trace('Fetching progress data from remote host');

                return this._replSystem.readFile(PROGRESS_FILE_NAME);
            })
            .then(
                action('lessonProgressLoadSuccess', (progressData) => {
                    _logger.silly('lessonProgressLoadSuccess()', progressData);

                    _logger.trace(
                        'Lesson and progress data loaded successfully'
                    );

                    try {
                        this._deserialize({
                            lessonData: JSON.parse(lessonData),
                            progressData: JSON.parse(progressData)
                        });
                        this._errorMessage = '';
                        this._isReady = true;
                    } catch (ex) {
                        _logger.error(
                            'Error parsing lesson/progress data',
                            ex.toString()
                        );
                        _logger.silly(ex);
                        this._errorMessage = ex.toString();
                    }
                    this._isBusy = false;
                })
            )
            .catch(
                action('lessonProgressLoadFailure', (err) => {
                    _logger.silly('lessonProgressLoadFailure()', err);

                    _logger.error(
                        'Error loading lesson/progress data',
                        err.toString()
                    );
                    this._isBusy = false;
                    this._errorMessage = err.toString();
                })
            );
    }

    /**
     * Saves lesson progress to a file on the remote REPL host.
     *
     * @param {String} data The progress data to store on the remote host.
     * @returns {Object} A promise that is rejected/resolved based on the
     *          outcome of the operation.
     */
    saveProgress() {
        _logger.silly('saveProgress()');

        if (this._lessons.length === 0) {
            _logger.debug('No lessons exist in cache. Skipping save progress');
            return;
        }

        if (this._isBusy) {
            _logger.warn('Load already in progress. Skipping');
            return;
        }

        _logger.trace('Serializing progress data');
        const lessonProgress = this._lessons.reduce(
            (lessonProgress, lesson) => {
                const data = lesson._serialize();
                lessonProgress[lesson.id] = {
                    lessonState: data.lessonState,
                    currentSectionId: data.currentSectionId
                };

                return lessonProgress;
            },
            {}
        );

        const progressData = {
            currentLessonId: this._currentLessonId,
            lessons: lessonProgress
        };

        _logger.trace('Storing progress to remote host');
        _logger.silly('Progress data', progressData);

        this._isBusy = true;
        this._errorMessage = '';

        return this._replSystem
            .writeFile(PROGRESS_FILE_NAME, JSON.stringify(progressData))
            .then(
                action('progressSaveSuccess', () => {
                    _logger.silly('progressSaveSuccess()');
                    _logger.trace('Progress data saved successfully');
                    this._isBusy = false;
                    this._errorMessage = '';
                })
            )
            .catch(
                action('progressSaveFailure()', (err) => {
                    _logger.silly('progressSaveFailure()');
                    _logger.error(
                        'Error loading lesson/progress data',
                        err.toString()
                    );
                    this._isBusy = false;
                    this._errorMessage = err.toString();
                })
            );
    }

    /**
     * Changes the current lesson selection.
     *
     * @param {String} lessonId The id of the lesson to be the next selection.
     */
    selectLesson(lessonId) {
        _logger.silly('selectLesson()', lessonId);

        const lesson = this._lessons.find((lesson) => lesson.id === lessonId);
        if (!lesson) {
            throw new Error('Lesson does not exist', lessonId);
        }
        if (lesson.notStarted) {
            throw new Error('Lesson not started yet', lessonId);
        }
        this._currentLessonId = lessonId;
        _logger.debug('Lesson selected', this._currentLessonId);
    }

    /**
     * Changes the lesson selection to the next lesson. This method will
     * have no effect if the currently selected lesson is the last lesson.
     */
    selectNextLesson() {
        _logger.silly('selectNextLesson()');

        if (this._lessons.length <= 0) {
            _logger.warn('There are no lessons to navigate');
            return;
        }

        if (this._currentLessonId < this._lessons.length - 1) {
            this._currentLessonId++;
            _logger.debug('Lesson selected', this._currentLessonId);

            const lesson = this._lessons[this._currentLessonId];
            if (lesson.notStarted) {
                lesson._start();
                _logger.debug('Lesson started', this._currentLessonId);
            }
        } else {
            _logger.warn('Cannot go beyond last lesson');
        }
    }

    /**
     * Changes the lesson selection to the previous lesson. This method will
     * have no effect if the currently selected lesson is the first lesson.
     */
    selectPreviousLesson() {
        _logger.silly('selectPreviousLesson()');

        if (this._lessons.length <= 0) {
            _logger.warn('There are no lessons to navigate');
            return;
        }

        if (this._currentLessonId > 0) {
            this._currentLessonId--;
        } else {
            _logger.warn('Cannot go prior to first lesson');
        }
    }
}

export default decorate(LessonListModel, {
    _errorMessage: observable,
    _isBusy: observable,
    _isReady: observable,
    _lessons: observable,
    _currentLessonId: observable,

    errorMessage: computed,
    isBusy: computed,
    lessons: computed,
    currentLesson: computed,
    currentLessonId: computed,
    hasNextLesson: computed,
    hasPreviousLesson: computed,
    isReady: computed,

    hasErrors: computed,

    _deserialize: action,

    load: action,
    saveProgress: action,
    selectLesson: action,
    selectNextLesson: action,
    selectPreviousLesson: action
});

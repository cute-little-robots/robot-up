import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import { getLogger } from '../utils/logger';

const _logger = getLogger('SectionModel');

/**
 * Represents a single section within a lesson. Each section is a teaching point
 * with some text and code annotations.
 */
class SectionModel {
    _id = -1;
    _title = '';
    _text = '';
    _annotations = [];

    /**
     * @param {String} id The id of the section.
     */
    constructor(id) {
        _logger.silly('ctor()', id);

        _argValidator.checkNumber(id, -1, 'Invalid id (arg #1)');

        this._id = id;
        this._title = '';
        this._text = '';
        this._annotations = [];
    }

    /**
     * The id of the section.
     *
     * @type {Number}
     */
    get id() {
        return this._id;
    }

    /**
     * The title of the section.
     *
     * @type {String}
     */
    get title() {
        return this._title;
    }

    /**
     * The text associated with the section, typically instructions or
     * information about a specific idea.
     *
     * @type {String}
     */
    get text() {
        return this._text;
    }

    /**
     * Returns an array of annotations that can be used to highlight specific
     * sections of code.
     *
     * @type {Array}
     */
    get annotations() {
        return this._annotations;
    }

    /**
     * Sets the properties of the section using the specified JSON object. This
     * is intended for internal consumption within the models, and should not be
     * invoked directly by the UI.
     *
     * @package (internal consumption only)
     * @param {Object} props An object that defines the properties of the
     *        section.
     *
     * @returns {Object} A reference to the current object.
     */
    _deserialize(props) {
        _logger.silly('_deserialize()', props);

        _argValidator.checkObject(props, 'Invalid props (arg #1)');
        _argValidator.checkString(
            props.title,
            1,
            'Invalid title (props.title)'
        );
        _argValidator.checkString(props.text, 1, 'Invalid title (props.text)');
        _argValidator.checkArray(
            props.annotations,
            'Invalid sections (props.annotations)'
        );

        this._title = props.title;
        this._text = props.text;
        this._annotations = props.annotations.slice();

        return this;
    }

    /**
     * Gets the properties of the section in JSON format. This is intended for
     * internal consumption within the models, and should not be invoked
     * directly by the UI.
     *
     * @package (internal consumption only)
     * @returns {Object} object that defines the properties of the section.
     */
    _serialize() {
        _logger.silly('_serialize()');

        return {
            id: this._id,
            title: this._title,
            text: this._text,
            annotations: this._annotations.slice()
        };
    }
}

export default decorate(SectionModel, {
    _id: observable,
    _title: observable,
    _text: observable,
    _annotations: observable,

    id: computed,
    title: computed,
    text: computed,
    annotations: computed,

    _deserialize: action
});

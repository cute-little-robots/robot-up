import { computed, observable, decorate } from 'mobx';

import ConnectionModel from './connection';
import TerminalModel from './terminal';
import LessonListModel from './lesson-list';
import { getLogger } from '../utils/logger';
import ReplSystem from '../data-access/repl-system';
import SocketManager from '../data-access/socket-manager';

const _logger = getLogger('ReplStore');

/**
 * MobX store for interaction with a remote WebRepl host. Supports connection,
 * an interactive terminal and the ability to execute actions supported by the
 * WebREPL protocol.
 *
 * See https://github.com/micropython/webrepl for more information on the
 * WebREPL protocol.
 */
class ReplStore {
    _connectionModel = undefined;
    _terminalModel = undefined;
    _lessonListModel = undefined;

    _socketManager = undefined;
    _replSystem = undefined;

    /**
     */
    constructor() {
        _logger.silly('ctor()');

        _logger.trace('Initializing data access');
        const socketManager = new SocketManager();
        const replSystem = new ReplSystem(socketManager);

        _logger.trace('Initializing models');
        const connectionModel = new ConnectionModel(socketManager);

        const terminalModel = new TerminalModel(socketManager, connectionModel);

        const lessonListModel = new LessonListModel(replSystem);

        _logger.trace('Setting member variables');
        this._socketManager = socketManager;
        this._replSystem = replSystem;

        this._connectionModel = connectionModel;
        this._terminalModel = terminalModel;

        this._lessonListModel = lessonListModel;
    }

    /**
     * Reference to the WebRepl connection model.
     *
     * @type {Object}
     */
    get connection() {
        return this._connectionModel;
    }

    /**
     * Reference to the WebRepl terminal model
     *
     * @type {Object}
     */
    get terminal() {
        return this._terminalModel;
    }

    /**
     * Reference to the lesson list.
     *
     * @type {Object}
     */
    get lessonList() {
        return this._lessonListModel;
    }

    /**
     * Reference to the repl control module.
     *
     * @type {Object}
     */
    get replSystem() {
        return this._replSystem;
    }
}

export default decorate(ReplStore, {
    _connectionModel: observable,
    _terminalModel: observable,
    _lessonListModel: observable,

    connection: computed,
    terminal: computed,
    lessonList: computed
});

import { argValidator as _argValidator } from '@vamship/arg-utils';
import { computed, action, observable, decorate } from 'mobx';

import { getLogger } from '../utils/logger';

const _logger = getLogger('FieldModel');

/**
 * Represents an input field, and provides actions that allow the updating of
 * the field value, and the applying client side validations.
 */
class FieldModel {
    _value = '';
    _errorMessage = '';
    _validateField = () => '';

    /**
     * @param {*} value The initial value to assign to the field.
     * @param {Function} [validateField] An optional validation function that
     *        can be used to check if the value conforms to validation rules for
     *        the field. If omitted, validation will not be performed, and any
     *        value will be accepted.
     */
    constructor(value, validateField) {
        _logger.silly('ctor()', value, validateField);

        this._value = value;
        if (_argValidator.checkFunction(validateField)) {
            this._validateField = validateField;
        }
    }

    /**
     * Returns the current value of the field.
     *
     * @type {*}
     */
    get value() {
        return this._value;
    }

    /**
     * Returns the error message associated with the field.
     *
     * @type {*}
     */
    get errorMessage() {
        return this._errorMessage;
    }

    /**
     * Returns the latest validation result for the field.
     *
     * @type {Boolean}
     */
    get hasErrors() {
        return this._errorMessage.length > 0;
    }

    /**
     * Update the current value of the field, and applies necessary validations.
     *
     * @param {String} value The new Value to apply to this field.
     * @param {Boolean} [skipValidation=false] If set to true, skips validation
     *        when the field is updated. This can be useful if validations need
     *        to be performed after several characters are collected during
     *        incremental update routines.
     * @returns {Boolean} True if the validation succeeded, false otherwise.
     */
    update(value, skipValidation) {
        _logger.silly('update()', value, skipValidation);

        this._value = value;
        return !skipValidation ? this.validate() : true;
    }

    /**
     * Performs validations on the current field value. If any errors are found,
     * the error message field is updated with the appropriate message.
     *
     * @returns {Boolean} True if the validation succeeded, false otherwise.
     */
    validate() {
        _logger.silly('validate()');

        const message = this._validateField(this._value);

        if (!_argValidator.checkString(message, 0)) {
            _logger.warn(
                'Error message was not a string, treating validation as successful.'
            );
            this._errorMessage = '';
        } else {
            this._errorMessage = message;
        }

        return this.hasErrors;
    }
}

export default decorate(FieldModel, {
    _value: observable,
    _errorMessage: observable,

    value: computed,
    errorMessage: computed,
    hasErrors: computed,

    update: action,
    validate: action
});

import { argValidator as _argValidator } from '@vamship/arg-utils';

import { getLogger } from './logger';

const _logger = getLogger('validationUtils');

/**
 * Validates the input value and returns a non empty error message if it is not
 * a valid websocket url.
 *
 * @param {*} value The value to validate.
 * @returns {String} A non empty error message if validation fails, an empty
 *          string if validation succeeds.
 */
export function validateWsUrl(value) {
    _logger.silly('validateWsUrl()', value);

    if (!_argValidator.checkString(value, 1)) {
        return 'Please enter a valid websocket url';
    }

    const pattern = /wss?:\/\/([0-9]{1,3}\.){3}([0-9]{1,3}):([0-9]{2,4})$/;
    if (!value.match(pattern)) {
        return 'Invalid websocket url format';
    }
}

/**
 * Validates the input value and returns a non empty error message if it is not
 * a valid password (i.e., the value needs to be a non empty string of with
 * length of at least 4).
 *
 * @param {*} value The value to validate.
 * @returns {String} A non empty error message if validation fails, an empty
 *          string if validation succeeds.
 */
export function validatePassword(value) {
    _logger.silly('validateWsUrl()');

    if (!_argValidator.checkString(value, 1)) {
        return 'Please enter a valid password';
    }
    if (value.length < 4) {
        return 'Password is too small';
    }
}

import { argValidator as _argValidator } from '@vamship/arg-utils';

import WebReplCommand from './webrepl-command';
import { getLogger } from '../utils/logger';

const _logger = getLogger('GetVersionCommand');

/**
 * WebREPL command to fetch version from the REPL host. The resulting version
 * number is returned as a string.
 */
export default class GetVersionCommand extends WebReplCommand {
    /**
     * @override
     */
    constructor(sendData) {
        _logger.silly('ctor()', sendData);

        super(sendData);
        this._requestBuffer[2] = 3;
        this._name = 'GetVersion';
    }

    /**
     * @override
     */
    handleHostResponse(data) {
        _logger.silly('handleHostResponse()', data && data.length);

        _argValidator.checkInstance(data, Uint8Array, 'Invalid data (arg #1)');

        const version = data.join('');
        _logger.trace('Received version number');
        _logger.silly('Version number', version);

        this._resolve(version);
    }
}

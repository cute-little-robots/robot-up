import { argValidator as _argValidator } from '@vamship/arg-utils';

import WebReplCommand from './webrepl-command';
import { getLogger } from '../utils/logger';

const _logger = getLogger('WriteFileCommand');

/**
 * WebREPL command to write data to a file on the REPL host.
 */
export default class WriteFileCommand extends WebReplCommand {
    _fileData = undefined;

    /**
     * @override
     */
    constructor(sendData) {
        _logger.silly('ctor()', sendData);

        super(sendData);
        this._requestBuffer[2] = 1;
        this._name = 'WriteFile';
        this._responseProcessor = this._processInitialResponse.bind(this);
    }

    /**
     * Processes the initial response from the remote repl host, and sends data
     * to the host one chunk at a time.
     *
     * @private
     * @param {Uint8Array} data The data from the remote host.
     */
    _processInitialResponse(data) {
        _logger.silly('_processInitialResponse()', data && data.length);

        const code = this._getResponseCode(data);
        if (code === 0) {
            _logger.debug('Sending file data to the server');

            for (
                let offset = 0;
                offset < this._fileData.length;
                offset += 1024
            ) {
                this._sendData(this._fileData.subarray(offset, offset + 1024));
            }

            _logger.debug('All data sent. Waiting for final acknowledgement');
            this._responseProcessor = this._processFinalResponse.bind(this);
        } else {
            this._reject(
                new Error(`Bad response code from remote host: ${code}`)
            );
        }
    }

    /**
     * Processes the final response from the remote repl host
     *
     * @private
     * @param {Uint8Array} data The data from the remote host.
     */
    _processFinalResponse(data) {
        _logger.silly('_processFinalResponse()');

        const code = this._getResponseCode(data);
        this._responseProcessor = undefined;
        if (code === 0) {
            _logger.debug('Final response received. File save completed');
            this._resolve();
        } else {
            this._reject(
                new Error(`Bad response code from remote host: ${code}`)
            );
        }
    }

    /**
     * @override
     */
    handleHostResponse(data) {
        _logger.silly('handleHostResponse()');

        _argValidator.checkInstance(data, Uint8Array, 'Invalid data (arg #1)');

        if (typeof this._responseProcessor === 'function') {
            this._responseProcessor(data);
        } else {
            _logger.warn(
                'No response processor available to handle host response'
            );
        }
    }

    /**
     * Sets file name and data to store on the remote host.
     *
     * @param {String} fileName The filename to retrieve from the remote host.
     * @param {String} fileData Data to write to the file as a string.
     */
    setFileData(fileName, fileData) {
        _logger.silly('setFileData()');

        _logger.silly(fileData);
        _argValidator.checkString(fileName, 1, 'Invalid fileName (arg #1)');
        _argValidator.checkString(fileData, 0, 'Invalid fileData (arg #2)');

        if (!this.isReady) {
            _logger.warn('Web REPL command is not ready. Skipping');
            return;
        }

        this._fileData = new TextEncoder('utf-8').encode(fileData);
        const dataLength = this._fileData.length;

        _logger.trace('File to fetch', fileName);
        _logger.trace('File data', dataLength);

        WebReplCommand._writeInt32(this._requestBuffer, 12, dataLength);
        this._setRequestTarget(fileName);
    }
}

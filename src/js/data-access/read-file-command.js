import { argValidator as _argValidator } from '@vamship/arg-utils';

import WebReplCommand from './webrepl-command';
import { getLogger } from '../utils/logger';

const _logger = getLogger('ReadFileCommand');

/**
 * WebREPL command to fetch the contents of a single file from the REPL host.
 * The resulting data is returned as a string.
 */
export default class ReadFileCommand extends WebReplCommand {
    _fileData = undefined;

    /**
     * @override
     */
    constructor(sendData) {
        _logger.silly('ctor()', sendData);

        super(sendData);
        this._requestBuffer[2] = 2;
        this._name = 'ReadFile';
        this._responseProcessor = this._processInitialResponse.bind(this);
    }

    /**
     * Processes the initial response from the remote repl host.
     *
     * @private
     * @param {Uint8Array} data The data from the remote host.
     */
    _processInitialResponse(data) {
        _logger.silly('_processInitialResponse()', data && data.length);

        const code = this._getResponseCode(data);
        if (code === 0) {
            _logger.debug('Acknowledging initial response');
            this._responseProcessor = this._processFileData.bind(this);
            this._fileData = new Uint8Array(0);
            this._sendData(new Uint8Array(1));
        } else {
            this._reject(
                new Error(`Bad response code from remote host: ${code}`)
            );
        }
    }

    /**
     * Processes file data from the repl host.
     *
     * @private
     * @param {Uint8Array} data The data from the remote host.
     */
    _processFileData(data) {
        _logger.silly('_processFileData()', data && data.length);

        const size = WebReplCommand._readInt16(data, 0);
        data = data.subarray(2);

        if (size === 0) {
            _logger.debug('All data received. Finalizing data fetch.');
            this._responseProcessor = this._processFinalResponse.bind(this);
        } else {
            _logger.trace('Copying file chunk');
            const buf = new Uint8Array(this._fileData.length + size);
            buf.set(this._fileData);
            buf.set(data, this._fileData.length);
            this._fileData = buf;
            this._sendData(new Uint8Array(1));
        }
    }

    /**
     * Processes the final response from the remote repl host
     *
     * @private
     * @param {Uint8Array} data The data from the remote host.
     */
    _processFinalResponse(data) {
        _logger.silly('_processFinalResponse()', data && data.length);

        const code = this._getResponseCode(data);
        this._responseProcessor = undefined;
        if (code === 0) {
            const data = new TextDecoder('utf-8').decode(this._fileData);

            _logger.trace('File fetch completed');
            _logger.silly('File data', data);
            this._resolve(data);
        } else {
            this._reject(
                new Error(`Bad response code from remote host: ${code}`)
            );
        }
    }

    /**
     * @override
     */
    handleHostResponse(data) {
        _logger.silly('handleHostResponse()', data && data.length);

        _argValidator.checkInstance(data, Uint8Array, 'Invalid data (arg #1)');

        if (typeof this._responseProcessor === 'function') {
            this._responseProcessor(data);
        } else {
            _logger.warn(
                'No response processor available to handle host response'
            );
        }
    }

    /**
     * Sets the file name to be retrieved from the remote host.
     *
     * @param {String} fileName The filename to retrieve from the remote host.
     */
    setFileName(fileName) {
        _logger.silly('setFileName()', fileName);

        _argValidator.checkString(fileName, 1, 'Invalid filename (arg #1)');

        if (!this.isReady) {
            _logger.warn('Web REPL command is not ready. Skipping');
            return;
        }

        _logger.trace('File to fetch', fileName);
        this._setRequestTarget(fileName);
    }
}

import { argValidator as _argValidator } from '@vamship/arg-utils';
import { Promise } from 'bluebird';

import { getLogger } from '../utils/logger';

const _logger = getLogger('WebReplCommand');

const RECORD_SIZE = 2 + 1 + 1 + 8 + 4 + 2 + 64;
const REQUEST_PREFIX_BYTES = ['W'.charCodeAt(0), 'A'.charCodeAt(0)];
const RESPONSE_PREFIX_BYTES = ['W'.charCodeAt(0), 'B'.charCodeAt(0)];

/**
 * Base class for command that can be performed on a micropython WEBREPL host.
 * This is an abstract class that must be extended by each concrete command that
 * can be performed on the host.
 */
export default class WebReplCommand {
    _request = undefined;
    _promise = undefined;
    _resolve = undefined;
    _reject = undefined;
    _sendData = undefined;
    _isReady = undefined;
    _name = undefined;

    /**
     * @param {Function} sendData A function that can be used to send data to
     *        the repl host.
     */
    constructor(sendData) {
        _logger.silly('ctor()', sendData);

        _argValidator.checkFunction(sendData, 'Invalid sendData (arg #1)');

        this._requestBuffer = new Uint8Array(RECORD_SIZE);
        REQUEST_PREFIX_BYTES.forEach(
            (byte, index) => (this._requestBuffer[index] = byte)
        );

        this._sendData = sendData;

        this._promise = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });

        this._isReady = true;
        this._name = 'WebReplCommand';
    }

    /**
     * Reference to the promise associated with this command.
     *
     * @type {Object}
     */
    get promise() {
        return this._promise;
    }

    /**
     * The name of the command.
     *
     * @type {String}
     */
    get name() {
        return this._name;
    }

    /**
     * Determines if the command is ready for execution.
     *
     * @type {Boolean}
     */
    get isReady() {
        return this._isReady;
    }

    /**
     * Writes a 16 bit number to the buffer at the specified offset. The data is
     * written in little endian format.
     *
     * @protected
     *
     * @param {Uint8Array} buf The byte array into which the data must be
     *        written
     * @param {Number} offset The offset at which the data must be written.
     * @param {Number} value The value to write.
     */
    static _writeInt16(buf, offset, value) {
        _logger.silly('_writeInt16()', buf, offset, value);

        _argValidator.checkInstance(
            buf,
            Uint8Array,
            'Invalid byte array (arg #1)'
        );
        _argValidator.checkNumber(offset, 0, 'Invalid offset (arg #2)');

        buf[offset] = value & 0xff;
        buf[offset + 1] = (value >> 8) & 0xff;
    }

    /**
     * Writes a 32 bit number to the buffer at the specified offset. The data is
     * written in little endian format.
     *
     * @protected
     *
     * @param {Uint8Array} buf The byte array into which the data must be
     *        written
     * @param {Number} offset The offset at which the data must be written.
     * @param {Number} value The value to write.
     */
    static _writeInt32(buf, offset, value) {
        _logger.silly('_writeInt32()', buf, offset, value);

        _argValidator.checkInstance(
            buf,
            Uint8Array,
            'Invalid byte array (arg #1)'
        );
        _argValidator.checkNumber(offset, 0, 'Invalid offset (arg #2)');

        buf[offset] = value & 0xff;
        buf[offset + 1] = (value >> 8) & 0xff;
        buf[offset + 2] = (value >> 16) & 0xff;
        buf[offset + 3] = (value >> 24) & 0xff;
    }

    /**
     * Reads a 16 bit number from the buffer at the specified offset. The data
     * is assumed to be in little endian format.
     *
     * @protected
     *
     * @param {Uint8Array} buf The byte array into which the data must be
     *        written
     * @param {Number} offset The offset at which the data must be written.
     *
     * @return {Number} The 16 bit number read from the offset.
     */
    static _readInt16(buf, offset) {
        _logger.silly('_readInt16()', buf, offset);

        _argValidator.checkInstance(
            buf,
            Uint8Array,
            'Invalid byte array (arg #1)'
        );
        _argValidator.checkNumber(offset, 0, 'Invalid offset (arg #2)');
        return buf[offset] | (buf[offset + 1] << 8);
    }

    /**
     * Writes a string value into the buffer at the specified offset. The data
     * is written one byte at a time by using the char code of each string
     * character.
     *
     * @protected
     *
     * @param {Uint8Array} buf The byte array into which the data must be
     *        written
     * @param {Number} offset The offset at which the data must be written.
     * @param {String} value The value to write.
     */
    static _writeString(buf, offset, value) {
        _logger.silly('_writeString()', buf, offset, value);

        _argValidator.checkInstance(
            buf,
            Uint8Array,
            'Invalid byte array (arg #1)'
        );
        _argValidator.checkNumber(offset, 0, 'Invalid offset (arg #2)');
        _argValidator.checkString(value, 0, 'Invalid value (arg #3)');

        for (let index = 0; index < value.length; index++) {
            buf[offset + index] = value.charCodeAt(index);
        }
    }

    /**
     * Inspects the response message, and returns the response code from bytes
     * 2 and 3. If the response is not valid -1 is returned.
     *
     * @protected
     *
     * @param {Uint8Array} data An array of bytes received from the repl host.
     * @returns {Number} The response code from the repl host.
     */
    _getResponseCode(data) {
        _logger.silly('_getResponseCode()', data && data.length);

        const isResponseValid = RESPONSE_PREFIX_BYTES.reduce(
            (result, byte, index) => result && byte === data[index],
            true
        );

        _logger.trace('Response header valid?', isResponseValid);

        if (isResponseValid) {
            _logger.trace('Response received from REPL host');
            return WebReplCommand._readInt16(data, 2);
        } else {
            _logger.warn('Bad response from REPL host');
            return -1;
        }
    }

    /**
     * Writes target information into the request buffer. This is typically the
     * name of the file that has to be retrieved or stored from the remote
     * repl host.
     *
     * @protected
     *
     * @param {String} target The target of the remote operation (typically
     *        the filename).
     */
    _setRequestTarget(target) {
        _logger.silly('_setRequestTarget()', target);

        _argValidator.checkString(target, 1, 'Invalid request target (arg #1)');

        _logger.trace('Setting target file in request buffer', target);
        WebReplCommand._writeInt16(this._requestBuffer, 16, target.length);
        WebReplCommand._writeString(this._requestBuffer, 18, target);
    }

    /**
     * Aborts command execution. This is intended for consumption internally,
     * and should not be directly invoked by the UI.
     *
     * @package (Internal consumption only)
     * @param {Object} error The error associated with the abort request.
     */
    _abort(error) {
        _logger.silly('_abort()', error);

        this._reject(error);
    }

    /**
     * Kicks off the command on the web repl host.
     *
     * @return {Object} The promise associated with the web repl command.
     */
    start() {
        _logger.silly('start()');

        if (!this._isReady) {
            _logger.warn('Web REPL command is not ready. Skipping');
            return;
        }

        this._isReady = false;
        this._sendData(this._requestBuffer);

        return this._promise;
    }

    /**
     * Processes response from the REPL host. Receives an array of bytes, and
     * performs necessary command based on the received response.
     *
     * @param {Uint8Array} data An array of bytes that contains the response
     *        from the host.
     */
    handleHostResponse(data) {
        _logger.silly('handleHostResponse()', data && data.length);
        _logger.warn('Child class does not implement handleHostResponse()');
    }

    /**
     * Processes error messages from the REPL host. This typically means that
     * the command is terminated with failure, though child classes can choose
     * to provide alternative implementations.
     *
     * @param {Object} error The error message received from the host.
     */
    handleHostError(error) {
        _logger.silly('handleHostError()', error);
        _logger.warn('Terminating command');

        this._reject(new Error(error));
    }
}

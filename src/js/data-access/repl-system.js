import { argValidator as _argValidator } from '@vamship/arg-utils';
import { Promise } from 'bluebird';

import GetVersionCommand from './get-version-command';
import ReadFileCommand from './read-file-command';
import WriteFileCommand from './write-file-command';
import WebReplCommand from './webrepl-command';
import { getLogger } from '../utils/logger';
import SocketManager from './socket-manager';

const _logger = getLogger('ReplSystem');

/**
 * Entity that supports the execution of commands supported by the WebREPL
 * protocol by exchanging binary messages with a remote host.
 */
export default class ReplSystem {
    _currentCommand = undefined;
    _commandPromise = undefined;

    /**
     * @param {Object} socketManager An abstract class that allows the
     *        creation/management of a socket connection.
     */
    constructor(socketManager) {
        _logger.silly('ctor()', socketManager);

        _argValidator.checkInstance(
            socketManager,
            SocketManager,
            'Invalid socket manager (arg #1)'
        );

        this._commandPromise = Promise.resolve();
        this._socketManager = socketManager;

        this._socketManager.on(
            'binaryData',
            this._processCommandData.bind(this)
        );

        this._socketManager.on('error', this._processError.bind(this));
        this._socketManager.on('close', this._processError.bind(this));

        this._sendDataMethod = (data) => this._socketManager.sendData(data);
    }
    /**
     *
     * Determines if the repl controller is ready for use.
     *
     * @type {Boolean}
     */
    get isReady() {
        return this._socketManager.isConnected;
    }

    /**
     * Processes socket errors and unexpected socket closures by allowing the
     * command to take remedial action.
     *
     * @private
     * @param {*} event The data associated with the event.
     */
    _processError(event) {
        _logger.silly('_processError()', event);

        _logger.warn('Host reported error while executing command');
        if (this._currentCommand instanceof WebReplCommand) {
            _logger.trace('Delegating error to custom handler');
            this._currentCommand.handleHostError(event);
        } else {
            _logger.warn('Ignoring host error. No active command exists');
        }
    }

    /**
     * Processes binary data received from the repl host. This typically
     * consists of responses to WebREPL file transfer/board control protocol
     * commands.
     *
     * See https://github.com/micropython/webrepl for more information.
     *
     * @private
     * @param {Uint8Array} data An array of bytes representing the data from the
     *        repl host.
     */
    _processCommandData(data) {
        _logger.silly('_processCommandData()', data && data.length);
        _logger.silly('Command data received', data);

        if (this._currentCommand instanceof WebReplCommand) {
            _logger.trace('Delegating processing to custom handler');
            this._currentCommand.handleHostResponse(data);
        } else {
            _logger.warn('Binary data received, but no command is in progress');
        }
    }

    /**
     * Schedules an command for execution. Ensures that any existing command are
     * completed before scheduling a new one.
     *
     * @private
     * @param {Object} command The command to schedule.
     * @return {Object} A promise that is resolved/rejected based on the
     *         outcome of the command.
     */
    _scheduleCommand(command) {
        _logger.silly('_scheduleCommand()', command);
        _logger.trace('Scheduling commmand for execution', command.name);

        this._commandPromise = this._commandPromise.finally(() => {
            if (!this.isReady) {
                return command._abort(
                    new Error('Cannot run command - connection is not ready')
                );
            }

            _logger.debug('Executing command on REPL host', command.name);
            this._currentCommand = command;
            return command
                .start()
                .catch((err) => {
                    _logger.warn('Handling rejected promise');
                    _logger.silly(err);
                })
                .finally(() => {
                    _logger.info('Command completed', command.name);
                    this._currentCommand = undefined;
                });
        });

        return command.promise;
    }

    /**
     * Retrieves the current web repl version number.
     *
     * @return {Object} A promise that is resolved/rejected based on the outcome
     *         of the version fetch operation. If resovled, the resolution will
     *         include version information.
     */
    getVersion() {
        _logger.silly('getVersion()');

        const command = new GetVersionCommand(this._sendDataMethod);
        return this._scheduleCommand(command);
    }

    /**
     * Reads a file from the web repl host.
     *
     * @param {String} fileName The filename to read.
     *
     * @return {Object} A promise that is resolved/rejected based on the outcome
     *         of the version fetch operation. If resovled, the resolution will
     *         contain the file data.
     */
    readFile(fileName) {
        _logger.silly('readFile()', fileName);
        _argValidator.checkString(fileName, 1, 'Invalid fileName (arg #1)');

        _logger.trace('File requested', fileName);
        const command = new ReadFileCommand(this._sendDataMethod);
        command.setFileName(fileName);

        return this._scheduleCommand(command);
    }

    /**
     * Writes a file to the web repl host.
     *
     * @param {String} fileName The filename to save the data as.
     * @param {String} fileData The file data to save.
     *
     * @return {Object} A promise that is resolved/rejected based on the outcome
     *         of the version fetch operation.
     */
    writeFile(fileName, fileData) {
        _logger.silly('writeFile()', fileName, fileData && fileData.length);

        _argValidator.checkString(fileName, 1, 'Invalid fileName (arg #1)');
        _argValidator.checkString(fileData, 0, 'Invalid fileData (arg #2)');

        _logger.trace('Saving file', fileName, fileData.length);
        const command = new WriteFileCommand(this._sendDataMethod);
        command.setFileData(fileName, fileData);

        return this._scheduleCommand(command);
    }
}

import { argValidator as _argValidator } from '@vamship/arg-utils';
import { EventEmitter } from 'events';

import { getLogger } from '../utils/logger';

import 'tachyons';

const _logger = getLogger('SocketManager');

/**
 * Lightweight wrapper around a socket object providing abstracted access to
 * communication with the remote WebREPL host.
 */
export default class SocketManager extends EventEmitter {
    /**
     */
    constructor() {
        _logger.silly('ctor()');

        super();
        this._socket = undefined;
    }

    /**
     *
     * Determines if the socket is connected and ready for use.
     *
     * @type {Boolean}
     */
    get isConnected() {
        return !!this._socket;
    }

    /**
     * Creates a new socket and establishes a connection with a remote WebRepl
     * host. This method will throw an error if invoked after a socket has been
     * created.
     *
     * @param {String} url The url of the remote host to connect to.
     * @param {String} password The password to send to the remote host after
     *        connection.
     */
    connect(url, password) {
        _logger.silly('connect()', url, password);

        _argValidator.checkString(url, 1, 'Invalid url (arg #1)');
        _argValidator.checkString(password, 1, 'Invalid password (arg #2)');

        if (this._socket) {
            throw new Error(
                'Cannot open socket. A socket has already been initialized.'
            );
        }

        _logger.debug('Opening connection to remote host', url);
        this._socket = new WebSocket(url);
        this._socket.binaryType = 'arraybuffer';

        _logger.trace('Setting up event handlers');
        this._socket.addEventListener('open', () => {
            _logger.debug('Connection to remote host opened', url);

            _logger.info('Sending password to server');
            this._socket.send(`${password}\r`);

            this.emit('open');
        });

        this._socket.addEventListener('close', () => {
            _logger.debug('Connection to remote host closed', url);
            this._socket = undefined;
            this.emit('close');
        });

        this._socket.addEventListener('error', (error) => {
            _logger.warn('Error reported by remote host', url);
            this.emit('error', error);
        });

        this._socket.addEventListener('message', (event) => {
            if (event.data instanceof ArrayBuffer) {
                _logger.silly('Binary data received from host');
                this.emit('binaryData', new Uint8Array(event.data));
            } else {
                _logger.silly('String data received from host');
                this.emit('stringData', event.data);
            }
        });
    }

    /**
     * Closes the socket connection to the remote WebRepl host. This method will
     * throw an error if
     */
    close() {
        _logger.silly('close()');

        if (!this._socket) {
            throw new Error('Cannot close socket. Socket not opened.');
        }

        _logger.debug('Closing connection to remote host');
        this._socket.close();
    }

    /**
     * Sends data to the REPL host. This method assumes that a valid socket
     * connection has already been established and will throw an error
     * otherwise.
     *
     * @param {String} data The data to send to the REPL host.
     */
    sendData(data) {
        _logger.silly('sendData()', data && data.length);

        if (!this._socket) {
            throw new Error('Cannot send data. Socket not opened.');
        }

        _logger.trace('Sending data to remote host');
        this._socket.send(data);
    }
}

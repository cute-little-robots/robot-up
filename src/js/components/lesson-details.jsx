import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { autorun } from 'mobx';

import { getLogger } from '../utils/logger';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faCaretLeft,
    faCaretRight,
    faHourglassHalf
} from '@fortawesome/free-solid-svg-icons';

const _logger = getLogger('LessonDetailsComponent');

/**
 * Component to view details of a lesson.
 */
class LessonDetails extends Component {
    _sectionAnimation = '';

    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()');
        super(props);

        // Selects the next section within the lesson.
        this._handleGotoNextSection = () => {
            _logger.silly('_handleGotoNextSection()');

            const { lesson } = this.props;
            this._sectionAnimation = 'fadeInRight';
            lesson.selectNextSection();
            _logger.trace('Next section selected');
        };

        // Selects the previous section within the lesson.
        this._handleGotoPreviousSection = () => {
            _logger.silly('_handleGotoPreviousSection()');

            const { lesson } = this.props;
            this._sectionAnimation = 'fadeInLeft';
            lesson.selectPreviousSection();
            _logger.trace('Previous section selected');
        };

        autorun(() => {
            _logger.silly('* autorun() [lesson updated]');
            if (this.props.lesson) {
                _logger.trace('Loading sections [force=false]');
                this.props.lesson.loadDetails();
            } else {
                _logger.warn('Skipping section load. Lesson not set.');
            }
        });
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const {
            lesson,
            isLastLesson,
            isFirstLesson,
            gotoNextLesson,
            gotoPreviousLesson
        } = this.props;
        let section;
        if (lesson) {
            section = lesson.currentSection;
        }

        const hasNextSection = lesson && lesson.hasNextSection;
        const hasPreviousSection = lesson && lesson.hasPreviousSection;
        const hasNextLesson = !hasNextSection && !isLastLesson;
        const hasPreviousLesson = !hasPreviousSection && !isFirstLesson;

        const activeSecondaryButtonStyle =
            'bg-secondary hover-bg-secondary-accent text-secondary pointer ';
        const inactiveSecondaryButtonStyle = 'bg-disabled text-disabled ';

        const noop = () => undefined;
        const handleGotoNextSection = hasNextSection
            ? this._handleGotoNextSection
            : noop;
        const handleGotoPreviousSection = hasPreviousSection
            ? this._handleGotoPreviousSection
            : noop;

        return (
            <div className="bg-surface flex">
                {lesson &&
                    lesson.isBusy && (
                        <div className="w-100">
                            <div className="fr inline-flex items-center">
                                <small className="i disabled">
                                    Loading lesson
                                </small>
                                <FontAwesomeIcon
                                    icon={faHourglassHalf}
                                    className="secondary ph2 pv2 animated rotateIn infinite fr"
                                    size="lg"
                                />
                            </div>
                        </div>
                    )}
                {lesson &&
                    !lesson.isBusy && (
                        <div className="pa3 w-100">
                            <h1 className="f4 lh-copy primary bb b--secondary">
                                {lesson.title}
                            </h1>

                            {section && (
                                <div className="overflow-hidden">
                                    <div
                                        key={section.id}
                                        className={`${
                                            this._sectionAnimation
                                        } animated faster`}
                                    >
                                        <h3 className="secondary">
                                            {section.title}
                                        </h3>
                                        <div className="f6 lh-copy ph2 text-surface">
                                            {section.text}
                                        </div>
                                    </div>
                                    <div className="w-100 pt2 cf">
                                        <button
                                            className={`${
                                                hasPreviousSection
                                                    ? activeSecondaryButtonStyle
                                                    : inactiveSecondaryButtonStyle
                                            } fl bn br2 ph3 items-center outline-0 inline-flex pv1`}
                                            onClick={handleGotoPreviousSection}
                                        >
                                            <FontAwesomeIcon
                                                icon={faCaretLeft}
                                                className="pr2"
                                                size="2x"
                                            />
                                            Back
                                        </button>
                                        <button
                                            className={`${
                                                hasNextSection
                                                    ? activeSecondaryButtonStyle
                                                    : inactiveSecondaryButtonStyle
                                            } fr bn br2 ph3 items-center outline-0 inline-flex pv1`}
                                            onClick={handleGotoNextSection}
                                        >
                                            Next
                                            <FontAwesomeIcon
                                                icon={faCaretRight}
                                                className="pl2"
                                                size="2x"
                                            />
                                        </button>
                                    </div>
                                    {hasNextLesson && (
                                        <div className="w-100 pt2">
                                            <button
                                                className="bg-primary hover-bg-primary-accent text-primary pointer w-100 pv3 bn br2 outline-0"
                                                onClick={gotoNextLesson}
                                            >
                                                Next Lesson
                                            </button>
                                        </div>
                                    )}
                                    {hasPreviousLesson && (
                                        <div className="w-100 pt2">
                                            <button
                                                className="bg-primary hover-bg-primary-accent text-primary pointer w-100 pv3 bn br2 outline-0"
                                                onClick={gotoPreviousLesson}
                                            >
                                                Previous Lesson
                                            </button>
                                        </div>
                                    )}
                                </div>
                            )}
                            {!section && (
                                <div className="tc pa2 pt3 code f6 bg-disabled text-disabled">
                                    No sections available
                                </div>
                            )}
                        </div>
                    )}
                {!lesson && (
                    <div className="w-100 tc pa2 pt3 code f6 bg-disabled text-disabled">
                        No Lessons available
                    </div>
                )}
            </div>
        );
    }
}

export default observer(LessonDetails);

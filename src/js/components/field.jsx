import React, { Component } from 'react';

import { getLogger } from '../utils/logger';

const _logger = getLogger('FieldComponent');

/**
 * Component that represents a single input field, with support for custom
 * validation and update routines.
 */
export default class Field extends Component {
    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()', props);

        super(props);

        // Handle changes to the value of the input field.
        this._handleValueChanged = (e) => {
            _logger.silly('* _handleValueChanged()');

            const value = e.target.value;
            const { notifyUpdate } = this.props;

            if (notifyUpdate) {
                notifyUpdate(value);
            } else {
                _logger.warn('Input field has no update notification handler');
            }
        };

        /**
         * Handle the blur event on the input. Can be useful if validations need
         * to be performed on blur.
         */
        this._handleBlur = (e) => {
            _logger.silly('* _handleBlur()');

            const value = e.target.value;
            const { notifyBlur } = this.props;

            if (notifyBlur) {
                notifyBlur(value);
            } else {
                _logger.debug('Input field has no blur notification handler');
            }
        };
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const {
            label,
            value,
            errorMessage,
            type,
            isEditable,
            fieldId
        } = this.props;

        const inputStyle = isEditable
            ? 'bg-background bn '
            : 'bg-surface ba b--primary ';

        return (
            <div className="mt3 cf">
                <label className="db fw6 lh-copy f6 pl1 pb1" htmlFor={fieldId}>
                    {label}
                </label>
                <input
                    className={`${inputStyle} input-reset pa3 lh-copy w-100 br2`}
                    name={fieldId}
                    id={fieldId}
                    type={type}
                    value={value}
                    disabled={!isEditable}
                    onChange={this._handleValueChanged}
                    onBlur={this._handleBlur}
                />
                {errorMessage && (
                    <small className="error fr pt2">{errorMessage}</small>
                )}
            </div>
        );
    }
}

import React, { Component } from 'react';
import { observer } from 'mobx-react';

import FieldComponent from './field';
import FieldModel from '../models/field';
import { getLogger } from '../utils/logger';
import { validateWsUrl, validatePassword } from '../utils/validation-utils';

const _logger = getLogger('ConnectionComponent');

/**
 * Component that manages the connection to the WebRepl host. Provides actions
 * to allow the client to connect/disconnect to/from the host.
 */
class Connection extends Component {
    _fields = [];
    _url = undefined;
    _password = undefined;

    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()', props);

        super(props);

        this._url = new FieldModel(props.defaultUrl, validateWsUrl);
        this._password = new FieldModel('', validatePassword);
        this._fields = [this._url, this._password];
    }

    /**
     * Returns a value changed handler for the specified field model.
     *
     * @private
     * @param {Object} field Reference to the field model for which the handler
     *        will be generated.
     *
     * @returns {Function} A handler function that handles the data changed
     *          event.
     */
    _getFieldChangedHandler(field) {
        _logger.silly('_getFieldChangedHandler()', field);

        return (value) => {
            _logger.silly('* _handleFieldChanged()');
            return field.update(value, true);
        };
    }

    /**
     * Handle changes to the password field.
     *
     * @private
     * @param value The updated value of the password field.
     *
     * @returns {Function} A handler function that handles the blur event.
     */
    _getFieldBlurHandler(field) {
        _logger.silly('_getFieldBlurHandler()', field);

        return () => {
            _logger.silly('* _handleFieldBlur()');
            return field.validate();
        };
    }

    /**
     * Handles a connect/disconnect action to the REPL host. The connection is
     * established/disconnected based on the current state of the connection.
     *
     * @param {Object} event The event object associated with the event.
     */
    _handleConnectionAction(event) {
        _logger.silly('_handleConnectionAction()', event);

        const { connection } = this.props;
        try {
            const hasErrors = this._fields.reduce(
                (result, field) => field.validate() || result,
                false
            );

            if (hasErrors) {
                _logger.debug('Validations failed. Not attempting connection');
            } else if (!connection.isReady) {
                _logger.debug('Opening connection');
                connection.open(this._url.value, this._password.value);
            } else {
                _logger.warn('Connection is already open');
            }
        } finally {
            event.preventDefault();
        }
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const { connection } = this.props;
        const url = this._url;
        const password = this._password;

        const handleUrlChanged = this._getFieldChangedHandler(this._url);
        const handleUrlBlur = this._getFieldBlurHandler(this._url);

        const handlePasswordChanged = this._getFieldChangedHandler(
            this._password
        );
        const handlePasswordBlur = this._getFieldBlurHandler(this._password);

        const handleConnectionAction = this._handleConnectionAction.bind(this);

        const { errorMessage } = connection;
        const isEditable = !connection.isReady && !connection.isConnecting;

        const buttonStyle = isEditable
            ? 'bg-secondary hover-bg-secondary-accent text-secondary grow bn pointer '
            : 'bg-disabled text-disabled ba ';
        const buttonText = connection.isConnecting
            ? 'Connecting ...'
            : 'Connect';

        return (
            <main>
                <section className="dt w-100 w-80-m w-75-l center pv5-l pv4-m">
                    <div className="bg-surface br2 pa3 pv4-ns w-60-l w-75-m w-100 center">
                        <fieldset className="bn">
                            <FieldComponent
                                label="Host Url"
                                fieldId="host-url"
                                type="text"
                                value={url.value}
                                errorMessage={url.errorMessage}
                                notifyUpdate={handleUrlChanged}
                                notifyBlur={handleUrlBlur}
                                isEditable={isEditable}
                            />
                            <FieldComponent
                                label="Password"
                                fieldId="password"
                                type="password"
                                value={password.value}
                                errorMessage={password.errorMessage}
                                notifyUpdate={handlePasswordChanged}
                                notifyBlur={handlePasswordBlur}
                                isEditable={isEditable}
                            />
                            <div className="mt3 pr2 cf">
                                <button
                                    className={`${buttonStyle} fr f6 f5-l button-reset pv3 tc w-100 w-40-m w-30-l br2`}
                                    onClick={handleConnectionAction}
                                    disabled={!isEditable}
                                >
                                    {buttonText}
                                </button>
                            </div>
                            <div>
                                {errorMessage && (
                                    <small className="error fr pt2">
                                        {errorMessage}
                                    </small>
                                )}
                            </div>
                        </fieldset>
                    </div>
                </section>
            </main>
        );
    }
}

export default observer(Connection);

import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { action, decorate, observable } from 'mobx';

import { getLogger } from '../utils/logger';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faTerminal } from '@fortawesome/free-solid-svg-icons';

const _logger = getLogger('TerminalComponent');

/**
 * Simple terminal component. Accepts data from the user one line at a time, and
 * dispatches each line to a remote host.
 */
class Terminal extends Component {
    _currentLine = '';
    _hadFocus = true;

    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()', props);

        super(props);

        /**
         * Handle changes to the current line on the terminal. These changes will
         * only be sent to the host once an enter key press is detected.
         */
        this._handleLineChanged = (event) => {
            _logger.silly('* _handleLineChanged()', event);

            this._currentLine = event.target.value;
        };

        // Handles the focus event, tracking when the terminal has focus.
        this._handleFocus = () => {
            _logger.silly('* _handleFocus()');

            this._hadFocus = true;
            _logger.trace('Had focus changed to', this._hadFocus);
        };

        // Handles the blur event, tracking when the terminal has focus.
        this._handleBlur = () => {
            _logger.silly('_handleBlur()');

            this._hadFocus = false;
            _logger.trace('Had focus changed to', this._hadFocus);
        };

        /**
         * Handle the key down event so that an enter key press can be detected.
         * This key indicates that the data from current line buffer has to be sent
         * to the host.
         *
         * If the enter key is not pressed, the data entered by the user is buffered
         * in anticipation of the enter key press.
         */
        this._handleKeyPress = (event) => {
            _logger.silly('* _handleKeyPress()', event);

            if (event.key === 'Enter') {
                const { terminal } = this.props;
                const data = this._currentLine + '\r';

                _logger.debug('Sending data to host');
                _logger.trace('Data to send', data);

                terminal.sendData(data);

                this._currentLine = '';
            }
        };
    }

    /**
     * Renders the terminal component.
     */
    render() {
        _logger.silly('render()');

        const { terminal, onClose } = this.props;

        return (
            <div className="bg-surface">
                <div className="relative pv3 ph5">
                    <FontAwesomeIcon
                        icon={faTerminal}
                        className="primary absolute"
                        size="lg"
                        style={{ left: '12px' }}
                    />
                    <span className="lh-title dib b pt1 primary">
                        REPL Terminal
                    </span>
                    <FontAwesomeIcon
                        icon={faTimes}
                        className="secondary hover-secondary-accent absolute pointer"
                        size="lg"
                        style={{ right: '12px' }}
                        onClick={onClose}
                    />
                </div>
                {terminal.isReady && (
                    <div className="bg-dark-gray white pa2 pt3 code f6 overflow-scroll vh-75">
                        {terminal.lines.map((data, index) => {
                            if (index < terminal.lines.length - 1) {
                                return (
                                    <div className="w-100" key={index}>
                                        {data}
                                    </div>
                                );
                            } else {
                                return (
                                    <div className="flex" key={index}>
                                        <div className="pr2">{data}</div>
                                        <input
                                            className="bg-dark-gray white input-reset outline-0 bn mb2 w-100"
                                            type="text"
                                            autoFocus={this._hadFocus}
                                            onFocus={this._handleFocus}
                                            onBlur={this._handleBlur}
                                            value={this._currentLine}
                                            onChange={this._handleLineChanged}
                                            onKeyPress={this._handleKeyPress}
                                            disabled={!terminal.isReady}
                                        />
                                    </div>
                                );
                            }
                        })}
                    </div>
                )}
                {!terminal.isReady && (
                    <div className="tc pa2 pt3 code f6 bg-disabled text-disabled">
                        REPL terminal is not ready
                    </div>
                )}
            </div>
        );
    }
}

export default observer(
    decorate(Terminal, {
        _currentLine: observable,
        _hadFocus: observable,

        _handleFocus: action,
        _handleBlur: action,
        _handleLineChanged: action,
        _handleKeyPress: action
    })
);

import React, { Component } from 'react';

import { observer } from 'mobx-react';

import ReplStore from '../models/repl-store';
import ConnectionComponent from './connection';
import StudentWorkspaceComponent from './student-workspace';
import FooterComponent from './footer';
import HeaderComponent from './header';

import { getLogger } from '../utils/logger';

const _logger = getLogger('AppComponent');

const store = new ReplStore();

/**
 * Root component for the web application.
 */
class App extends Component {
    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()');

        super(props);

        // Closes the connection to the remote repl host.
        this._handleCloseConnection = () => {
            _logger.silly('* _handleCloseConnection()');
            _logger.trace('Closing connection to repl host');
            store.connection.close();
        };
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const { connection, terminal, lessonList } = store;
        const showConnection = connection.isReady;

        return (
            <div className="text-background bg-background h-100">
                <HeaderComponent />
                {!showConnection && (
                    <ConnectionComponent
                        defaultUrl="ws://192.168.4.1:8266"
                        connection={connection}
                    />
                )}
                {showConnection && (
                    <StudentWorkspaceComponent
                        connection={connection}
                        terminal={terminal}
                        lessonList={lessonList}
                        onQuit={this._handleCloseConnection}
                    />
                )}
                <FooterComponent />
            </div>
        );
    }
}
export default observer(App);

import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faMehBlank
} from '@fortawesome/free-solid-svg-icons';

import { getLogger } from '../utils/logger';

const _logger = getLogger('HeaderComponent');

/**
 * Root component for the web application.
 */
class Header extends Component {
    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        return (
            <header className="bb bw3 b--primary-accent w-100 pv1 ph3 bg-primary text-primary inline-flex items-center">
                <div className="pa1 pr2 tc dib">
                    <FontAwesomeIcon
                        icon={faMehBlank}
                        className="bn secondary b--secondary"
                        size="lg"
                    />
                </div>
                <h1 className="b dib f5 f4-m f3-l fw2 mv2">Robot Up</h1>
            </header>
        );
    }
}

export default Header;

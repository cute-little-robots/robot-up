import React, { Component } from 'react';

import { getLogger } from '../utils/logger';

const _logger = getLogger('FooterComponent');

/**
 * Root component for the web application.
 */
class Footer extends Component {
    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        // const linkStyle = 'f6 dib ph2 link secondary dim';
        return (
            <footer className="pv4 ph3 ph5-m ph6-l primary bg-surface bt bw2 b--surface-accent mt1">
                <small className="f6 db tc">
                    © 2019 <b className="">Robot Up</b>
                </small>
                { /*
                <div className="tc mt3 dn">
                    <a href="/language/" title="Language" className={linkStyle}>
                        Language
                    </a>
                    <a href="/terms/" title="Terms" className={linkStyle}>
                        Terms of Use
                    </a>
                    <a href="/privacy/" title="Privacy" className={linkStyle}>
                        Privacy
                    </a>
                </div>
                */ }
            </footer>
        );
    }
}

export default Footer;

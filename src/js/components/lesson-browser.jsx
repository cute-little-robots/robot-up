import React, { Component } from 'react';
import { observer } from 'mobx-react';

import { getLogger } from '../utils/logger';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faCircle,
    faDotCircle,
    faCheckCircle,
    faGraduationCap,
    faTimes
} from '@fortawesome/free-solid-svg-icons';

const _logger = getLogger('LessonBrowserComponent');

/**
 * Root component for the web application.
 */
class LessonBrowser extends Component {
    /**
     * Return the appropriate icon for a lesson item.
     *
     * @private
     * @param lesson Reference to the lesson item that is being styled.
     *
     * @returns {Object} Reference to the react component to use as the icon.
     */
    _getItemIcon(lesson) {
        _logger.silly('_getItemIcon()', lesson);

        const { lessonList } = this.props;

        if (lesson.id === lessonList.currentLessonId) {
            return faDotCircle;
        } else if (lesson.isComplete) {
            return faCheckCircle;
        } else {
            return faCircle;
        }
    }

    /**
     * Return the appropriate style for a lesson item.
     *
     * @private
     * @param lesson Reference to the lesson item that is being styled.
     * @param itemType Determines the item type - primary or secondary.
     *
     * @returns {String} CSS class names to apply to the item.
     */
    _getItemStyle(lesson, itemType) {
        _logger.silly('_getItemStyle()', lesson, itemType);

        const { lessonList } = this.props;
        const type = itemType === 'primary' ? 'primary' : 'secondary';

        const style = lesson.notStarted
            ? 'disabled '
            : `${type} hover-${type}-accent pointer `;

        return lesson.id === lessonList.currentLessonId ? `${style}b bg-surface-accent ` : `${style} bg-surface `;
    }

    /**
     * Returns a handler that changes the current lesson selection.
     *
     * @private
     * @param lesson Reference to the lesson item that has been selected.
     *
     * @returns {Function} A handler function that changes the current lesson
     *          selection.
     */
    _getLessonSelectedHandler(lesson) {
        _logger.silly('_getLessonSelectedHandler()', lesson);

        return () => {
            _logger.silly('* _handleLessonSelected()', lesson);
            if(lesson.notStarted) {
                _logger.debug('Cannot select lesson that has not yet started');
                return;
            }

            this.props.lessonList.selectLesson(lesson.id);
            _logger.trace('Lesson selected', lesson.id);
            this.props.onClose();
        };
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const { lessonList, onClose } = this.props;

        return (
            <div className="toc">
                <span className="pv3 ph5 flex items-baseline no-underline relative bg-surface">
                    <div className="track-line-start" />
                    <FontAwesomeIcon
                        icon={faGraduationCap}
                        className="primary absolute"
                        size="2x"
                        style={{ left: '12px' }}
                    />
                    <span className="lh-title dib b pt1 primary">Lessons</span>
                    <FontAwesomeIcon
                        icon={faTimes}
                        className="secondary hover-secondary-accent absolute pointer"
                        size="lg"
                        style={{ right: '12px' }}
                        onClick={onClose}
                    />
                </span>
                {lessonList.lessons.map((lesson, index) => (
                    <button
                        className={`${this._getItemStyle(
                            lesson,
                            'primary'
                        )} bn w-100 pv3 ph5 flex items-baseline no-underline relative outline-0`}
                        key={lesson.id}
                        onClick={this._getLessonSelectedHandler(lesson)}
                    >
                        <div className="track-line-top" />
                        {index < lessonList.lessons.length - 1 && (
                            <div className="track-line-bottom" />
                        )}
                        <FontAwesomeIcon
                            className="absolute"
                            icon={this._getItemIcon(lesson)}
                            style={{ left: '25px' }}
                        />
                        <span
                            className={`${this._getItemStyle(
                                lesson,
                                'secondary'
                            )} f7 mr2 mono tracked`}
                        >
                            {lesson.id}
                        </span>
                        <span className="title dib b">{lesson.title}</span>
                    </button>
                ))}
            </div>
        );
    }
}

export default observer(LessonBrowser);

import React, { Component } from 'react';

import { Promise } from 'bluebird';
import { observer } from 'mobx-react';
import { action, autorun, decorate, observable } from 'mobx';
import AceEditor from 'react-ace';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faBan,
    faDownload,
    faExclamation,
    faHourglassHalf,
    faPlay,
    faHistory,
    faToggleOff,
    faToggleOn
} from '@fortawesome/free-solid-svg-icons';

import 'brace/mode/python';
import 'brace/theme/monokai';

import { getLogger } from '../utils/logger';

const _logger = getLogger('EditorComponent');

/**
 * A text editor component that allows users to interactively change text/code.
 */
class Editor extends Component {
    _showMarkers = true;
    _isRunning = false;
    _hasChanges = false;
    _tooltipMessage = '';

    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()', props);

        super(props);
        this._editorRef = React.createRef();

        // Toggle show marker mode - enable or disable markers on the editor.
        this._handleToggleMarkers = () => {
            _logger.silly('* _handleToggleMarkers()');

            this._showMarkers = !this._showMarkers;
            _logger.trace('Show marker flag changed to', this._showMarkers);
        };

        // Toggle run mode - run if the program is not running, stop if it is.
        this._handleToggleRun = () => {
            _logger.silly('* _handleToggleRun()');

            if (this._isRunning) {
                _logger.trace('Stopping program');
                this.props.stop();
                this._isRunning = false;
            } else {
                return this._handleSaveData().then(
                    action('_handleSaveDataSuccess', () => {
                        _logger.silly('_handleSaveDataSuccess()');
                        _logger.trace('Starting program');

                        this.props.run();
                        this._isRunning = true;
                    })
                );
            }
        };

        // Handle changes to editor content.
        this._handleContentChanged = (data) => {
            _logger.silly('* _handleContentChanged()', data && data.length);

            const { file } = this.props;

            if (file) {
                _logger.silly('Updating file contents');
                this._hasChanges = true;
                file.setContents(data);
            } else {
                _logger.warn(
                    'Skipping file content updates. File model not set'
                );
            }
        };

        // Handle request to load data from the repl host.
        this._handleLoadData = () => {
            _logger.trace('* _handleLoadData()');

            const { file } = this.props;

            if (file) {
                _logger.trace('Loading file contents [force=true]');
                file.load(true);
            } else {
                _logger.warn('Skipping file load. File model not set');
            }
        };

        // Handle request to save data to the repl host.
        this._handleSaveData = () => {
            _logger.trace('* _handleSaveData()');

            const { file } = this.props;

            if (!file) {
                _logger.warn('Skipping file save. File model not set');
                return Promise.resolve();
            }

            if (!this._hasChanges) {
                _logger.warn('Skipping file save. No changes to save');
                return Promise.resolve();
            }
            _logger.trace('Saving file contents');
            return file.save().then(
                action('fileSaveSuccess', () => {
                    this._hasChanges = false;
                })
            );
        };

        // Clears the tool tip text for a given icon
        this._clearTooltip = () => {
            _logger.trace('* _clearTooltip()');
            this._tooltipMessage = '';
        };

        // Shows tooltip for the error icon
        this._showErrorTooltip = () => {
            _logger.trace('* _showErrorTooltip()');

            if (this.props.file) {
                _logger.trace('Setting error message as the tooltip');
                this._tooltipMessage = this.props.file.errorMessage;
            } else {
                _logger.warn('Skipping setting error message tooltip');
            }
        };

        // Shows tooltip for the run button
        this._showRunTooltip = this._generateTooltipHandler(
            'showRunTooltip',
            'Run your code on the robot'
        );

        // Shows tooltip for the run button
        this._showStopTooltip = this._generateTooltipHandler(
            'showStopTooltip',
            'Stop running your code on the robot'
        );

        // Shows tooltip for the download button
        this._showDownloadTooltip = this._generateTooltipHandler(
            'showDownloadTooltip',
            'Download your code to the robot'
        );

        // Shows tooltip for the restore button
        this._showRestoreTooltip = this._generateTooltipHandler(
            'showRestoreTooltip',
            'Start over with the code from the robot'
        );

        // Shows tooltip for the enable marker button
        this._showEnableMarkerTooltip = this._generateTooltipHandler(
            'showEnableMarkerTooltip',
            'Show code highlights'
        );

        // Shows tooltip for the disable marker button
        this._showDisableMarkerTooltip = this._generateTooltipHandler(
            'showDisableMarkerTooltip',
            'Hide code highlights'
        );

        autorun(() => {
            _logger.silly('* autorun() [file ref updated]');
            if (this.props.file) {
                _logger.trace('Loading file contents [force=false]');
                this.props.file.load();
            } else {
                _logger.warn('Skipping file load. File model not set');
            }
        });
    }

    /**
     * Generates a handler that shows a tooltip.
     * @private
     *
     * @param {String} name A name for the tooltip handler.
     * @param {String} message The tooltip message to show.
     * @return {Function} A handler that can be bound to events.
     */
    _generateTooltipHandler(name, message) {
        return action(name, () => {
            _logger.silly(`* ${name}()`, message);
            this._tooltipMessage = message;
        });
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const { markers, file } = this.props;

        const noop = () => undefined;
        const isReadOnly = !file || file.isBusy;
        const hasErrors = file && file.hasErrors;
        const showTooltip = !!this._tooltipMessage;

        const iconStyle = isReadOnly
            ? 'disabled '
            : 'secondary hover-secondary-accent pointer ';

        return (
            <div className="bg-surface">
                <div className="bb b--surface-accent cf dt w-100">
                    <FontAwesomeIcon
                        icon={this._isRunning ? faBan : faPlay}
                        className={`${iconStyle} ph2 pv2`}
                        size="lg"
                        onClick={isReadOnly ? noop : this._handleToggleRun}
                        onMouseOver={
                            this._isRunning
                                ? this._showStopTooltip
                                : this._showRunTooltip
                        }
                        onMouseOut={this._clearTooltip}
                    />
                    <div className={`${iconStyle} dib relative`}>
                        <FontAwesomeIcon
                            icon={faDownload}
                            className="ph2 pv2"
                            size="lg"
                            onClick={isReadOnly ? noop : this._handleSaveData}
                            onMouseOver={this._showDownloadTooltip}
                            onMouseOut={this._clearTooltip}
                        />
                        {this._hasChanges && (
                            <span className={`absolute right-0 ${iconStyle}`}>
                                *
                            </span>
                        )}
                    </div>
                    <FontAwesomeIcon
                        icon={faHistory}
                        className={`${iconStyle} ph2 pv2`}
                        size="lg"
                        onClick={isReadOnly ? noop : this._handleLoadData}
                        onMouseOver={this._showRestoreTooltip}
                        onMouseOut={this._clearTooltip}
                    />
                    <FontAwesomeIcon
                        icon={this._showMarkers ? faToggleOn : faToggleOff}
                        className={`${iconStyle} ph2 pv2`}
                        size="lg"
                        onClick={isReadOnly ? noop : this._handleToggleMarkers}
                        onMouseOver={
                            this._showMarkers
                                ? this._showDisableMarkerTooltip
                                : this._showEnableMarkerTooltip
                        }
                        onMouseOut={this._clearTooltip}
                    />
                    {file &&
                        file.isBusy && (
                            <FontAwesomeIcon
                                icon={faHourglassHalf}
                                className="secondary ph2 pv2 animated rotateIn infinite fr"
                                size="lg"
                            />
                        )}
                    {showTooltip && (
                        <div className="dtc v-mid dib f6 i tr pr2 primary-accent">
                            {this._tooltipMessage}
                        </div>
                    )}
                    {!isReadOnly &&
                        hasErrors && (
                            <FontAwesomeIcon
                                icon={faExclamation}
                                className="error ph2 pv2 fr"
                                onMouseOver={this._showErrorTooltip}
                                onMouseOut={this._clearTooltip}
                                size="sm"
                            />
                        )}
                </div>
                <div className="relative">
                    <AceEditor
                        ref={this._editorRef}
                        mode="python"
                        theme="monokai"
                        width="100%"
                        height="640px"
                        fontSize="14pt"
                        name="UNIQUE_ID_OF_DIV"
                        editorProps={{ $blockScrolling: 'Infinity' }}
                        value={file ? file.contents : ''}
                        onChange={this._handleContentChanged}
                        readOnly={isReadOnly}
                        markers={this._showMarkers ? markers : []}
                    />
                </div>
            </div>
        );
    }
}

export default observer(
    decorate(Editor, {
        _showMarkers: observable,
        _isRunning: observable,
        _hasChanges: observable,
        _tooltipMessage: observable,

        _handleContentChanged: action,
        _handleToggleMarkers: action,
        _handleToggleRun: action,
        _clearTooltip: action,
        _showErrorTooltip: action
    })
);

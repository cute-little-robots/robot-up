import React, { Component } from 'react';

import { observer } from 'mobx-react';
import { action, decorate, observable, when } from 'mobx';

import TerminalComponent from './terminal';
import EditorComponent from './editor';
import LessonBrowserComponent from './lesson-browser';
import LessonDetailsComponent from './lesson-details';
import { getLogger } from '../utils/logger';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faExclamation,
    faGraduationCap,
    faHourglassHalf,
    faSignOutAlt,
    faTerminal,
    faSave
} from '@fortawesome/free-solid-svg-icons';

const _logger = getLogger('StudentWorkspaceComponent');

/**
 * Root component for the web application.
 */
class StudentWorkspace extends Component {
    _showLessonBrowser = false;
    _showTerminal = false;
    _tooltipMessage = '';

    /**
     * @override
     */
    constructor(props) {
        _logger.silly('ctor()');

        super(props);

        this._showLessonBrowser = false;
        this._showTerminal = false;

        // Toggles the visibility of the lesson browser.
        this._handleToggleLessonBrowser = () => {
            _logger.silly('* _handleToggleLessonBrowser()');

            this._showLessonBrowser = !this._showLessonBrowser;
            this._tooltipMessage = '';
            this._showTerminal = false;
        };

        // Toggles the visibility of the terminal.
        this._handleToggleTerminal = () => {
            _logger.silly('* _handleToggleTerminal()');

            this._showLessonBrowser = false;
            this._tooltipMessage = '';
            this._showTerminal = !this._showTerminal;
        };

        // Executes a run command on the repl host.
        this._handleRunCommand = () => {
            _logger.silly('* _handleRunCommand()');

            const { terminal } = this.props;
            terminal.sendRunCommand();
        };

        // Executes a stop command on the repl host.
        this._handleStopCommand = () => {
            _logger.silly('* _handleStopCommand()');

            const { terminal } = this.props;
            terminal.sendStopCommand();
        };

        // Saves current lesson progress.
        this._handleSaveProgress = () => {
            this.props.lessonList.saveProgress();
        };

        // Clears the tool tip text for a given icon
        this._clearTooltip = () => {
            _logger.trace('* _clearTooltip()');
            this._tooltipMessage = '';
        };

        // Shows tooltip for the save button
        this._showSaveTooltip = this._generateTooltipHandler(
            'showSaveTooltip',
            'Save your progress'
        );

        // Shows tooltip for the terminal button
        this._showTerminalTooltip = this._generateTooltipHandler(
            'showTerminalTooltip',
            'View REPL terminal'
        );

        // Shows tooltip for the lesson browser button
        this._showLessonBrowserTooltip = this._generateTooltipHandler(
            'showLessonBrowserTooltip',
            'View lesson list'
        );

        // Shows tooltip for the quit button
        this._showQuitTooltip = this._generateTooltipHandler(
            'showQuitTooltip',
            'Quit'
        );

        // Shows tooltip for the error icon
        this._showErrorTooltip = () => {
            _logger.trace('* _showErrorTooltip()');

            this._tooltipMessage = this.props.lessonList.errorMessage;
        };

        when(
            () => this.props.lessonList && !this.props.lessonList.isReady,
            () => {
                _logger.silly('* when() [connection ready]');
                this.props.lessonList.load();
            }
        );
    }

    /**
     * Extracts editor properties from the lesson object.
     *
     * @private
     * @param {Object} lesson The lesson object from which to extract
     *        properties.
     * @returns {Object} Reference to a code model and annotation data for the
     *          lesson
     */
    _getEditorProperties(lesson) {
        let markers = [];
        let file = undefined;
        if (lesson) {
            const { currentSection, code } = lesson;
            if (currentSection) {
                markers = currentSection.annotations.map((marker) => ({
                    startRow: marker.startRow,
                    startcol: marker.startCol,
                    endRow: marker.endRow,
                    endCol: marker.endCol,
                    className: 'code-annotation',
                    type: 'line'
                }));
            }
            file = code;
        }

        return { markers, file };
    }

    /**
     * Generates a handler that shows a tooltip.
     * @private
     *
     * @param {String} name A name for the tooltip handler.
     * @param {String} message The tooltip message to show.
     * @return {Function} A handler that can be bound to events.
     */
    _generateTooltipHandler(name, message) {
        return action(name, () => {
            _logger.silly(`* ${name}()`, message);
            this._tooltipMessage = message;
        });
    }

    /**
     * @override
     */
    render() {
        _logger.silly('render()');

        const { onQuit, terminal, lessonList } = this.props;

        const currentLesson = lessonList.currentLesson;
        const { markers, file } = this._getEditorProperties(currentLesson);

        const noop = () => undefined;
        const isReadOnly = lessonList.isBusy;
        const hasErrors = lessonList.hasErrors;
        const showTooltip = !!this._tooltipMessage;

        const iconStyle = isReadOnly
            ? 'disabled '
            : 'secondary hover-secondary-accent pointer ';

        return (
            <main className="flex flex-wrap">
                <div className="w-60-l w-100">
                    <EditorComponent
                        file={file}
                        markers={markers}
                        run={this._handleRunCommand}
                        stop={this._handleStopCommand}
                    />
                </div>
                <div className="w-40-l w-100 overflow-hidden">
                    {!this._showLessonBrowser &&
                        !this._showTerminal && (
                            <div className="cf bg-surface bb b--surface-accent dt w-100">
                                {showTooltip && (
                                    <div className="dtc v-mid dib f6 i pl2 tl primary-accent mw5 truncate">
                                        {this._tooltipMessage}
                                    </div>
                                )}
                                {!isReadOnly &&
                                    hasErrors && (
                                        <FontAwesomeIcon
                                            icon={faExclamation}
                                            className="error ph2 pv2 fr"
                                            size="sm"
                                            onMouseOver={this._showErrorTooltip}
                                            onMouseOut={this._clearTooltip}
                                        />
                                    )}
                                {isReadOnly && (
                                    <FontAwesomeIcon
                                        icon={faHourglassHalf}
                                        className="secondary ph2 pv2 animated rotateIn infinite fr"
                                        size="lg"
                                    />
                                )}
                                <FontAwesomeIcon
                                    icon={faSignOutAlt}
                                    className={`${iconStyle} ph2 pv2 fr`}
                                    size="lg"
                                    onClick={onQuit}
                                    onMouseOver={this._showQuitTooltip}
                                    onMouseOut={this._clearTooltip}
                                />
                                <FontAwesomeIcon
                                    icon={faGraduationCap}
                                    className={`${iconStyle} ph2 pv2 fr`}
                                    size="lg"
                                    onClick={
                                        isReadOnly
                                            ? noop
                                            : this._handleToggleLessonBrowser
                                    }
                                    onMouseOver={this._showLessonBrowserTooltip}
                                    onMouseOut={this._clearTooltip}
                                />
                                <FontAwesomeIcon
                                    icon={faTerminal}
                                    className={`${iconStyle} ph2 pv2 fr`}
                                    size="lg"
                                    onClick={
                                        isReadOnly
                                            ? noop
                                            : this._handleToggleTerminal
                                    }
                                    onMouseOver={this._showTerminalTooltip}
                                    onMouseOut={this._clearTooltip}
                                />
                                <FontAwesomeIcon
                                    icon={faSave}
                                    className={`${iconStyle} ph2 pv2 fr`}
                                    size="lg"
                                    onClick={
                                        isReadOnly
                                            ? noop
                                            : this._handleSaveProgress
                                    }
                                    onMouseOver={this._showSaveTooltip}
                                    onMouseOut={this._clearTooltip}
                                />
                            </div>
                        )}
                    {!this._showLessonBrowser &&
                        !this._showTerminal && (
                            <LessonDetailsComponent
                                lesson={currentLesson}
                                isLastLesson={!lessonList.hasNextLesson}
                                isFirstLesson={!lessonList.hasPreviousLesson}
                                gotoNextLesson={lessonList.selectNextLesson.bind(
                                    lessonList
                                )}
                                gotoPreviousLesson={lessonList.selectPreviousLesson.bind(
                                    lessonList
                                )}
                            />
                        )}
                    {!this._showLessonBrowser &&
                        this._showTerminal && (
                            <div className="animated fadeIn faster">
                                <TerminalComponent
                                    onClose={this._handleToggleTerminal}
                                    terminal={terminal}
                                />
                            </div>
                        )}
                    {this._showLessonBrowser &&
                        !this._showTerminal && (
                            <div className="animated slideInRight faster">
                                <LessonBrowserComponent
                                    onClose={this._handleToggleLessonBrowser}
                                    lessonList={lessonList}
                                />
                            </div>
                        )}
                </div>
            </main>
        );
    }
}
export default observer(
    decorate(StudentWorkspace, {
        _showLessonBrowser: observable,
        _showTerminal: observable,
        _tooltipMessage: observable,

        _handleToggleLessonBrowser: action,
        _handleToggleTerminal: action,
        _clearTooltip: action,
        _showErrorTooltip: action
    })
);

import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/components/app';
import * as serviceWorker from './js/serviceWorker';

import { configure } from 'mobx';

import 'animate.css';
import 'tachyons';
import './css/app.scss';

configure({ enforceActions: 'observed' });

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
